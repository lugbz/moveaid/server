# This file is part of the MoveAid project.
# Copyright (C) 2021 Marco Marinello <mmarinello@unibz.it>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""moveaid URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.conf import settings
from django.contrib import admin
from django.urls import path, include
from rest_framework.authtoken import views as Rviews
from django.conf.urls import url

from moveaid import views
from moveart import views as RTviews

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/auth/token', Rviews.obtain_auth_token),
    # path('api/web/', include('webapi.urls')),
    path('api/sms/', include('sms_office.urls')),
    path('api/companion/position/submit', RTviews.VehiclePositionSubmission.as_view()),
    path('api/companion/position/last', RTviews.LastUserPosition.as_view()),
    path('api/companion/verify', RTviews.VerifyToken.as_view()),
    path('accounts/', include('django.contrib.auth.urls')),
    path('', views.HomeView.as_view(), name="home"),
    path('locations/ajax_list', views.LocationsAjaxQuery.as_view(), name='locations-ajax-list'),
    path('addresses/ajax_search', views.AddressAjaxQuery.as_view(), name='addresses-ajax-search'),
    path('operators/', include(('operators.urls', 'operators'), 'operators')),
    url(r'^signup/$', views.Signup.as_view(), name='signup'),
]

if settings.DEBUG:
    urlpatterns.append(path('api/auth/', include('rest_framework.urls', namespace='rest_framework')))
