# This file is part of the MoveAid project.
# Copyright (C) 2021 Marco Marinello <marco@marinello.bz.it>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from django.core.management.base import BaseCommand

from moveaid.models import Location


class Command(BaseCommand):
    def handle(self, *args, **options):
        for loc in Location.objects.exclude(lat__isnull=True).filter(address_parts__isnull=True):
            print(loc.id, loc.address, "...")
            k = loc.get_coordinates_from_OSM()
            if k[0] != 0:
                print("\tOK ...", loc.address, *k)
            else:
                print("\tError.")
