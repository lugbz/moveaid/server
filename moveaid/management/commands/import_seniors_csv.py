# This file is part of the MoveAid project.
# Copyright (C) 2021 Marco Marinello <marco@marinello.bz.it>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from django.contrib.auth.models import User
from django.core.management.base import BaseCommand
import csv
from moveaid.models import Organization, Senior, Location
from moveaid.utils import sanitize_phone_number


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument("card_issuer", type=str)
        parser.add_argument("csv_source", type=str)

    def handle(self, card_issuer, csv_source, *args, **kwargs):
        print(card_issuer, csv_source)
        d_o = Organization.objects.get(name=card_issuer)
        with open(csv_source, newline="") as fh:
            reader = csv.reader(fh)
            for row in reader:
                print(row[0], row[2], row[1], end=" ... ")
                _u = User.objects.create(
                    username=f"{row[2].lower().replace(' ', '_')}_{row[1].lower().replace(' ', '_')}",
                    first_name=row[2].title(),
                    last_name=row[1].title(),
                )
                if row[8]:
                    _u.email = row[8].strip()
                    _u.save()
                Senior.objects.create(
                    user=_u,
                    phone_number=sanitize_phone_number(row[7]) if row[7] else None,
                    contact_email=bool(_u.email),
                    contact_sms=(bool(row[7]) and not row[7].startswith("04")) if row[7] else False,
                    member_card_issuer=d_o,
                    member_card_number=int(row[0]),
                    home_address=Location.objects.create(address=f"{row[5]} {row[3]}")
                )
                print("ok")
