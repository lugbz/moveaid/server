# This file is part of the MoveAid project.
# Copyright (C) 2021 Marco Marinello <mmarinello@unibz.it>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from django.contrib.auth.models import User
from django.core.management.base import BaseCommand
from datetime import datetime, date, timedelta, time
import random
from moveaid import models, utils


class Command(BaseCommand):
    def genTrip(self, senior, app_time):
        if not senior.senior_set.exists():
            senior.senior_set.create(
                phone_number=random.randint(311111111, 3999999999),
                home_address=random.choice(self.FAKE_HOMES),
                member_card_issuer=random.choice(models.Organization.objects.exclude(id=9999)),
                member_card_number=random.randint(100, 9999)
            )
        s_o = senior.senior_set.all()[0]
        r = models.Reservation.objects.create(
            senior=s_o,
            inserted_by=self.EMPLOYEE
        )
        start = models.TripStage.objects.create(
            reservation=r,
            location=s_o.home_address,
            number=0,
            estimated_be_at=app_time
        )
        end = models.TripStage.objects.create(
            reservation=r,
            location=random.choice(self.DESTINATIONS),
            number=1,
            estimated_be_at=app_time+timedelta(minutes=15)
        )
        models.TripLeg.objects.create(
            reservation=r,
            start=start,
            end=end
        )

    def randomDay(self, day, u):
        start_time = time(hour=8, minute=30)
        slot = timedelta(minutes=15)
        app_time = datetime.combine(day, start_time)
        print("app_time", app_time, "start_time", start_time)
        for i in range(8):
            for k in range(random.randint(0, 3)):
                self.genTrip(random.choice(u), app_time)
                app_time += slot
                print("app_time", app_time)

    def handle(self, *args, **options):
        utils.createOrganizations()
        utils.generateVehicles()
        self.FAKE_HOMES = [
            models.Location.objects.get_or_create(address=i[0], lat=i[1], lng=i[2])[0] for i in [
                ["Via Claudia Augusta, 73, 39100 Bolzano BZ", 46.482545459614705, 11.342329547715641],
                ["Via S. Vigilio, 75, 39100 Bolzano BZ", 46.482634, 11.346138],
                ["Via Santa Geltrude, 18, 39100 Bolzano BZ", 46.48773954792036, 11.347168196429273],
                ["Via Torino, 24, 39100 Bolzano BZ", 46.49169337612917, 11.34028785945792],
                ["Via Firenze, 4, 39100 Bolzano BZ", 46.49307455279849, 11.342927294916306],
                ["Via di Novacella, 14, 39100 Bolzano BZ", 46.49363801342926, 11.339857569964371],
                ["Via S. Quirino, 24, 39100 Bolzano BZ", 46.497311718329534, 11.344364611462433],
                ["Via della Mendola, 44-52, 39100 Bolzano BZ", 46.49896124622728, 11.334946094800525]
            ]
        ]
        self.DESTINATIONS = [
            models.Location.objects.get_or_create(name=i[0], address=i[1], lat=i[2], lng=i[3])[0] for i in [
                ["Ospedale Bolzano, ingresso principale", "Via Lorenz Böhler, 5, 39100 Bolzano BZ",
                 46.49880439205378, 11.308946879287776],
                ["Casa di Cura S. Maria", "Via Claudia de' Medici, 2, 39100 Bolzano BZ",
                 46.501733628695504, 11.351650042819664],
                ["OrthoPlus", "Via Talvera, 2c, 39100 Bolzano BZ",
                 46.50189611826646, 11.348978559030105],
                ["Villa Melitta", "Via Col di Lana, 6I, 39100 Bolzano BZ",
                 46.504678790292935, 11.342481828131035]
            ]
        ]
        u = User.objects.all()
        if len(u) < 500:
            u = utils.generateRandomUsers(500)
        e = models.Employee.objects.all()
        if not e.exists():
            e_u = utils.generateRandomUsers(1)[0]
            e = [models.Employee.objects.create(
                user=e_u,
                organization=models.Organization.objects.all()[0],
                is_operator=True
            )]
        self.EMPLOYEE = e[0]
        today = date.today()
        first_day = today - timedelta(days=today.weekday())
        day = first_day
        print(today, first_day, day)
        for i in range(5):
            self.randomDay(day, u)
            day += timedelta(days=1)
            print(day)
        day += timedelta(days=2)
        for i in range(5):
            self.randomDay(day, u)
            day += timedelta(days=1)
            print(day)
