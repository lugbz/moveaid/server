# This file is part of the MoveAid project.
# Copyright (C) 2023 Marco Marinello <mmarinello@unibz.it>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import csv
from io import StringIO

from datetime import datetime, timedelta

from django.conf import settings
from django.core.management.base import BaseCommand
from post_office import mail

from moveaid.models import TripStage, Reservation, TripLeg


def date_or_time_if_available(obj, date=True):
    if not obj:
        return "(non noto)"
    if date:
        return obj.strftime("%d/%m/%Y")
    return obj.strftime("%H:%M")


def get_first_day_of_month(dt):
    return dt.replace(day=1, hour=0, minute=0, second=0, microsecond=0)


class Command(BaseCommand):
    def handle(self, *args, **options):
        today = datetime.now()
        first_day_current_month = get_first_day_of_month(today)
        first_day_previous_month = get_first_day_of_month(today - timedelta(days=today.day))
        qs = TripStage.objects.filter(estimated_be_at__range=(first_day_previous_month, first_day_current_month))
        legs = TripLeg.objects.filter(reservation__pk__in=qs.values_list("reservation", flat=True))
        out = StringIO()
        writer = csv.writer(out)
        writer.writerow([
            "Cognome",
            "Nome",
            "Anno di nascita",
            "Giorno servizio",
            "Ora servizio",
            "Note"
        ])
        for i in legs:
            writer.writerow([
                i.reservation.senior.user.last_name,
                i.reservation.senior.user.first_name,
                i.reservation.senior.yob or "",
                date_or_time_if_available(i.start.estimated_be_at),
                date_or_time_if_available(i.start.estimated_be_at, False),
                i.reservation.notes or ""
            ])
        out.seek(0)
        mail.send(
            settings.REPORTING_EMAIL,
            subject="[MoveAid] Report mensile viaggi",
            message="Gentile Presidente,\nsi trasmette in allegato il report mensile delle corse in formato CSV.\n"
                    "Cordiali saluti,\nMoveAid",
            attachments={"report.csv": out}
        )
        return
