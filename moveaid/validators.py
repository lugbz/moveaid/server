# This file is part of the MoveAid project.
# Copyright (C) 2021 Andrea Esposito <aesposito@unibz.it>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _

import datetime

def check_future_date(date):
    if type(date) == datetime.datetime:
        date = date.date()
    if date < datetime.date.today():
        raise ValidationError(_("Make sure to select a future date."))

def check_trip_leg_list(trip_legs):
    for i, trip_leg in enumerate(trip_legs):
        if type(trip_leg) != dict or \
            not trip_leg.get('start') or \
                not trip_leg.get('end'):
            raise ValidationError(_("Make sure that each trip leg is complete with start and end points."))
        # FIXME: assuming start and end objects are valid
        start = datetime.time.fromisoformat(trip_leg['start']['estimated_be_at'])
        end = datetime.time.fromisoformat(trip_leg['end']['estimated_be_at'])
        if start > end:
            raise ValidationError(_("Make sure that departure times are set to be earlier than arrival times."))

        if i > 0:
            for j in range(0, i):
                prev = trip_legs[j]
                prev_start = datetime.time.fromisoformat(prev['start']['estimated_be_at'])
                prev_end = datetime.time.fromisoformat(prev['end']['estimated_be_at'])
                if prev_start < start < prev_end or start < prev_end < end:
                    raise ValidationError(_("Make sure trip leg times are not intersecting."))