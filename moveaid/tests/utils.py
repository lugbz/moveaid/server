# This file is part of the MoveAid project.
# Copyright (C) 2021 Andrea Esposito <aesposito@unibz.it>
# Copyright (C) 2021 Marco Marinello <mmarinello@unibz.it>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from django.contrib.auth.models import User
from django.test import TestCase
from moveaid import utils
from moveaid.models import Organization, Vehicle, Reservation


class OrganizationsCreationTestCase(TestCase):
    def setUp(self):
        utils.createOrganizations()

    def test_organizations_exist(self):
        for o in utils.ORGANIZATIONS:
            self.assertTrue(Organization.objects.filter(name=o), f'{o} cannot be found in test db.')


class RandomUsersGenerationTestCase(TestCase):
    def setUp(self):
        utils.generateRandomUsers(10)

    def test_random_users_generator(self):
        self.assertTrue(User.objects.all(), "users not in db after calling random generator")


class VehiclesGenerationTestCase(TestCase):
    def setUp(self):
        utils.createOrganizations()
        utils.generateVehicles()

    def test_vehicles_generator(self):
        self.assertTrue(Vehicle.objects.all(), "vehicles not in db after calling generator")


class RandomUsersWithRolesGenerationTestCase(TestCase):
    def setUp(self):
        utils.createOrganizations()

    def test_generate_random_users_with_senior_role(self):
        utils.generateRandomUsersWithRoles(4, ['senior'])
        self.assertEqual(User.objects.filter(
            senior__isnull=False,
            employee__isnull=True
        ).count(), 4, 'senior users count mismatch')

    def test_generate_random_users_with_driver_role(self):
        utils.generateRandomUsersWithRoles(4, ['driver'])
        self.assertEqual(User.objects.filter(
            senior__isnull=True,
            employee__isnull=False,
            employee__is_driver=True,
            employee__is_operator=False
        ).count(), 4, 'driver users count mismatch')

    def test_generate_random_users_with_operator_role(self):
        utils.generateRandomUsersWithRoles(4, ['operator'])
        self.assertEqual(User.objects.filter(
            senior__isnull=True,
            employee__isnull=False,
            employee__is_operator=True,
            employee__is_driver=False
        ).count(), 4, 'operator users count mismatch')

    def test_generate_random_users_with_driver_and_operator_role(self):
        utils.generateRandomUsersWithRoles(2, ['driver', 'operator'])
        self.assertEqual(User.objects.filter(
            senior__isnull=True,
            employee__isnull=False,
            employee__is_driver=True,
            employee__is_operator=True
        ).count(), 2, 'driver + operator users count mismatch')


class ReservationGenerationTestCase(TestCase):
    def setUp(self):
        utils.createOrganizations()
        utils.generateVehicles()
        utils.generateRandomUsersWithRoles(1, ['senior'])
        utils.generateRandomUsersWithRoles(1, ['operator'])
        utils.generateRandomUsersWithRoles(1, ['driver'])
        utils.generateReservation()

    def test_resevation_exists(self):
        self.assertTrue(Reservation.objects.all(), 'reservation not in db after calling generator')
        self.assertTrue(Reservation.objects.filter(tripstage__isnull=True).count() == 0, 'at least one reservation has no trip stages in db associated to it')