# This file is part of the MoveAid project.
# Copyright (C) 2021 Marco Marinello <mmarinello@unibz.it>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""
MoveAid admin.py
Register MoveAid models for handling from Django administration
"""

from django.contrib import admin
from moveaid import models


def register(model, **kw):
    """
    Register a Model into admin by automatically creating his ModelAdmin and
    filling it with values from keyword arguments
    
    :param model: The Model to be registered
    :param kw: Attributes to be added to the ModelAdmin class
    :return:
    """
    class MyModelAdmin(admin.ModelAdmin):
        pass
    for i in kw:
        setattr(MyModelAdmin, i, kw[i])
    admin.site.register(model, MyModelAdmin)


register(
    models.Organization,
    search_fields=["name"],
    list_display=["name"]
)

register(
    models.Employee,
    search_fields=["user__last_name", "user__first_name"],
    list_display=["user", "organization", "is_driver", "is_operator"],
    list_filter=["organization", "is_driver", "is_operator"]
)

register(
    models.Vehicle,
    search_fields=["plate_number", "car_model"],
    list_display=["plate_number", "car_model", "wheelchair", "owner"],
    list_filter=["owner", "wheelchair"]
)

register(
    models.DriverShift,
    search_fields=["vehicle__plate_number", "vehicle__car_model"],
    list_display=["driver", "time_start", "time_end", "vehicle"],
    list_filter=["driver__organization"]
)


@admin.action(description="Merge locations")
def merge_locations(modeladmin, request, queryset):
    a = queryset[0]
    b = queryset[1:]
    for i in b:
        i.tripstage_set.update(location=a)
        i.senior_set.update(home_address=a)
        i.delete()


register(
    models.Location,
    search_fields=["name", "address"],
    list_display=["name", "address", "lat", "lng"],
    actions=[merge_locations],
)

register(
    models.Senior,
    search_fields=["user__last_name", "user__first_name"],
    list_display=[
        "user",
        "phone_number",
        "member_card_issuer",
        "member_card_number",
        "home_address"
    ],
    list_filter=["member_card_issuer"],
    autocomplete_fields=["user"]
)

register(
    models.Reservation,
    search_fields=["senior__user__last_name", "senior__user__first_name"],
    list_display=[
        "senior",
        "first_stage_time",
        "needs_wheelchair",
        "inserted_by"
    ],
    autocomplete_fields=["senior"]
)

register(
    models.TripStage,
    list_display=[
        "reservation",
        "number",
        "location",
        "estimated_be_at"
    ]
)

register(
    models.TripLeg,
    list_display=[
        "reservation",
        "start",
        "end"
    ]
)
