# This file is part of the MoveAid project.
# Copyright (C) 2021 Andrea Esposito <aesposito@unibz.it>
# Copyright (C) 2021 Marco Marinello <mmarinello@unibz.it>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from django.contrib.auth.models import User
import random
import requests
import string
from datetime import datetime, timedelta
from moveaid.models import Organization, Vehicle, Employee, Senior, Location, Reservation, DriverShift, TripStage


ORGANIZATIONS = ["ADA", "Antea", "Auser"]


def createOrganizations():
    for o in ORGANIZATIONS:
        Organization.objects.get_or_create(name=o)


def generateRandomUsers(number):
    names = [a.title() for a in requests.get(
        "https://raw.githubusercontent.com/napolux/paroleitaliane/master/paroleitaliane/9000_nomi_propri.txt"
    ).text.split("\n")]
    surnames = requests.get(
        "https://raw.githubusercontent.com/napolux/paroleitaliane/master/paroleitaliane/lista_38000_cognomi.txt"
    ).text.split("\n")
    created_users = []
    for i in range(number):
        name = random.choice(names)
        surname = random.choice(surnames)
        created_users.append(User.objects.create(
            username=f"{name}{surname}{i}",
            first_name=name,
            last_name=surname,
            email=f"{name}{surname}{i}@movea.id"
        ))
    return created_users


def generateRandomUsersWithRoles(number, roles):
    users = generateRandomUsers(number)
    entities = []
    for user in users:
        if "senior" in roles:
            entities.append(Senior.objects.create(
                user=user,
                phone_number="".join([random.choice(string.digits) for _ in range(10)]),
                contact_sms=random.random() < 0.5,
                contact_email=random.random() < 0.5,
                member_card_issuer=random.choice(Organization.objects.all()),
                member_card_number=random.randint(100, 9999),
                language='it',
                home_address=Location.objects.create(
                    address='Viale Druso, 32'
                )
            ))
        if "driver" in roles or "operator" in roles:
            entities.append(Employee.objects.create(
                user=user,
                organization=Organization.objects.all().first(),
                is_driver="driver" in roles,
                is_operator="operator" in roles
            ))
    return entities


def generateVehicles():
    plate_index = 0
    for i in ORGANIZATIONS:
        Vehicle.objects.create(
            plate_number="{l}{l}{n}{n}{n}{l}{l}".format(**{"n": plate_index, "l": string.ascii_uppercase[plate_index]}),
            car_model="Virtual Dummy Car",
            wheelchair=i == "Antea",
            owner=Organization.objects.get(name=i)
        )
        plate_index += 1


def generateReservation(senior=None, inserted_by=None, driver=None):
    if not senior:
        senior = Senior.objects.all().first()
    if not inserted_by:
        inserted_by = Employee.objects.filter(is_operator=True).all().first()
    if not driver:
        driver = Employee.objects.filter(is_driver=True).all().first()
    r = Reservation.objects.create(
        senior=senior,
        inserted_by=inserted_by
    )
    d = DriverShift.objects.create(
        driver=driver,
        vehicle=Vehicle.objects.all().first(),
        time_start=(datetime.now() + timedelta(days=1)),
        time_end=(datetime.now() + timedelta(days=1, hours=6))
    )
    t = TripStage.objects.create(
        reservation=r,
        location=senior.home_address,
        number=1,
        estimated_be_at=d.time_start,
    )
    return r


def sanitize_phone_number(num):
    return num.replace("+39", "").replace(" ", "").replace("-", "")
