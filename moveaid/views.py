# This file is part of the MoveAid project.
# Copyright (C) 2021 Marco Marinello <marco.marinello@lugbz.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import requests
from django.conf import settings
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponseRedirect, JsonResponse
from django.views import View
from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import UserCreationForm
from django.shortcuts import render, redirect
from django.views.generic import TemplateView, FormView

from moveaid.models import Location


class HomeView(TemplateView):
    template_name = "landing_page.html"

    def get(self, *args, **kwargs):
        if self.request.user.is_authenticated:
            return HttpResponseRedirect("/operators/")
        return super().get(*args, **kwargs)


class LocationsAjaxQuery(LoginRequiredMixin, View):
    def get_queryset(self):
        return Location.objects.exclude(name=None)

    def to_select2_list(self, queryset):
        return [{
            "id": i.pk, 
            "pk": i.pk,
            "text": i.name,
            "name": i.name,
            "address": i.address,
            "address_parts": i.address_parts,
            "lat": i.lat,
            "lon": i.lng
        } for i in queryset]

    def get(self, request):
        return JsonResponse(self.to_select2_list(self.get_queryset()), safe=False)

    def post(self, request):
        query = request.POST.get("q")
        if not query:
            return self.get()
        return JsonResponse(self.to_select2_list(self.get_queryset().filter(name__icontains=query)), safe=False)


class AddressAjaxQuery(LoginRequiredMixin, View):
    def to_select2_list(self, queryset):
        out = []
        for el in queryset:
            el["id"] = el["display_name"]
            el["text"] = el["display_name"],
            out.append(el)
        return out

    def post(self, request):
        query = request.POST.get("q", "")
        if not query or len(query) < 6:
            return JsonResponse([], safe=False)
        try:
            q = requests.get(settings.NOMINATIM_QUERY_URL, params={
                "q": query,
                "format": "json",
                "addressdetails": "1"
            }).json()
            return JsonResponse(self.to_select2_list(q), safe=False)
        except IndexError:
            return JsonResponse([], safe=False)


class Signup(FormView):
    template_name = "registration/signup.html"
    form_class = UserCreationForm

    def form_valid(self, form):
        if not settings.OPEN_REGISTRATION:
            raise ValueError("Registration not allowed")
        instance = form.save()
        login(self.request, instance)
        return redirect('home')
