# This file is part of the MoveAid project.
# Copyright (C) 2021 Marco Marinello <marco.marinello@lugbz.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from django.conf import settings
import subprocess

try:
    GIT_REVISION = subprocess.check_output(
        ["git", "rev-parse", "--short", "HEAD"],
        cwd=settings.BASE_DIR
    ).strip().decode("utf-8")
except subprocess.CalledProcessError:
    GIT_REVISION = "unknown"

with open(settings.BASE_DIR / "VERSION") as vfile:
    VERSION = vfile.read().strip()


def system_info(request):
    return {
        "INFO_SYSTEM_GIT_REVISION": GIT_REVISION,
        "INFO_SYSTEM_VERSION": VERSION,
        "INFO_SYSTEM_INSTANCE_NAME": settings.INSTANCE_NAME,
        "INFO_SYSTEM_OPEN_REGISTRATION": settings.OPEN_REGISTRATION,
    }
