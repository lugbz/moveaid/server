# This file is part of the MoveAid project.
# Copyright (C) 2021 Marco Marinello <mmarinello@unibz.it>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
import base64
import os
import shutil
import subprocess
import tempfile
from io import BytesIO

from django.contrib.auth.models import User
from django.conf import settings
from django.db import models
from django.utils.translation import ugettext_lazy as _

import requests
import datetime

from odf import opendocument, userfield

from moveaid.context_processors import VERSION, GIT_REVISION


class Organization(models.Model):
    """
    This is the entity that owns the vehicles and to which the employees refer.
    """

    name = models.CharField(max_length=50, verbose_name=_("name"))

    __str__ = lambda self: self.name

    class Meta:
        verbose_name = _("Organization")
        verbose_name_plural = _("Organizations")
        ordering = ('name', )


class Employee(models.Model):
    """
    It describes the roles of volunteers/employees of organisations.
    """

    def get_dummy_organization():
        return Organization.objects.get_or_create(
            id=9999,
            name="Dummy organization"
        )[0].id

    user = models.ForeignKey(
        "auth.User",
        on_delete=models.PROTECT,
        verbose_name=_("user")
    )

    organization = models.ForeignKey(
        "moveaid.Organization",
        on_delete=models.PROTECT,
        verbose_name=_("organization"),
        default=get_dummy_organization
    )

    is_driver = models.BooleanField(
        default=False,
        verbose_name=_("is driver")
    )

    is_operator = models.BooleanField(
        default=False,
        verbose_name=_("is operator")
    )

    color = models.CharField(
        max_length=20,
        default="#121212",
    )

    __str__ = lambda self: '{} ({})'.format(
        self.user.get_full_name() or self.user.username,
        self.organization.name
    )

    class Meta:
        verbose_name = _("Employee")
        verbose_name_plural = _("Employees")
        ordering = ('user__last_name', 'user__first_name')


class Vehicle(models.Model):
    """
    It represents the vehicles owned by the various organisations.
    """

    plate_number = models.CharField(
        max_length=15,
        verbose_name=_("plate number")
    )

    car_model = models.TextField(verbose_name=_("model"))

    wheelchair = models.BooleanField(
        default=False,
        verbose_name=_("wheelchair"),
        help_text=_("this vehicle can transport a wheelchair")
    )

    owner = models.ForeignKey(
        "moveaid.Organization",
        on_delete=models.PROTECT,
        verbose_name=_("organization")
    )

    user = models.OneToOneField(
        "auth.User",
        on_delete=models.PROTECT,
        verbose_name=_("user"),
        null=True,
        blank=True
    )

    active = models.BooleanField(
        default=True,
        verbose_name=_("active"),
        help_text=_("whether the vehicle can currently be used or not")
    )

    __str__ = lambda self: f'{self.plate_number} {self.car_model}'

    def create_user(self):
        if not getattr(self, "user"):
            self.user = User.objects.create(username=f"vehicle-{self.plate_number}")
            self.save()

    def set_return_password(self):
        if not getattr(self, "user"):
            self.create_user()
        password = User.objects.make_random_password(length=20)
        self.user.set_password(password)
        self.user.save()
        self.save()
        return password

    def get_last_position_today(self):
        qs = self.vehicleposition_set.filter(timestamp__gt=datetime.datetime.now().date())
        if not qs.exists():
            return [None, None, None]
        lp = qs.first()
        return [lp.lat, lp.lng, lp.timestamp]

    class Meta:
        verbose_name = _("Vehicle")
        verbose_name_plural = _("Vehicles")
        ordering = ('owner', 'plate_number')


class DriverShift(models.Model):
    """
    Shifts and/or availability of single drivers.
    """

    driver = models.ForeignKey(
        "moveaid.Employee",
        on_delete=models.PROTECT,
        verbose_name=_("driver")
    )

    vehicle = models.ForeignKey(
        "moveaid.Vehicle",
        on_delete=models.PROTECT,
        verbose_name=_("vehicle"),
        null=True,
        blank=True
    )

    time_start = models.DateTimeField(
        verbose_name=_("start time")
    )

    time_end = models.DateTimeField(
        verbose_name=_("end time")
    )

    KM_displayed_start = models.BigIntegerField(
        verbose_name=_("KM displayed at start"),
        null=True,
        blank=True
    )

    KM_displayed_end = models.BigIntegerField(
        verbose_name=_("KM displayed at end"),
        null=True,
        blank=True
    )

    has_filled_oil = models.BooleanField(
        verbose_name=_("has filled oil"),
        default=False
    )

    @property
    def get_driver(self):
        return self.driver

    @property
    def get_vehicle(self):
        return self.vehicle

    def get_vehicle_plate(self):
        return getattr(getattr(self, "vehicle", None), "plate_number", None)

    __str__ = lambda self: f'{self.driver} {self.time_start} - {self.time_end}'

    class Meta:
        verbose_name = _("Driver shift")
        verbose_name_plural = _("Drivers shifts")
        ordering = ('time_start', )


class Location(models.Model):
    """
    It locates a certain spatial position on the map.
    Some positions may have a name to facilitate the use of the system.
    """

    name = models.TextField(
        verbose_name=_("name"),
        null=True,
        blank=True
    )

    address = models.TextField(
        verbose_name=_("address"),
        help_text=_("OSM address for this location")
    )

    address_parts = models.JSONField(
        verbose_name=_("address parts"),
        null=True,
        blank=True
    )

    lat = models.DecimalField(
        max_digits=9,
        decimal_places=6,
        null=True,
        blank=True,
        verbose_name=_("latitude")
    )

    lng = models.DecimalField(
        max_digits=9,
        decimal_places=6,
        null=True,
        blank=True,
        verbose_name=_("longitude")
    )

    def get_coordinates_from_OSM(self):
        try:
            q = requests.get(settings.NOMINATIM_QUERY_URL, params={
                "q": self.address,
                "format": "json",
                "addressdetails": "1"
            }).json()
            self.lat = float(q[0]["lat"])
            self.lng = float(q[0]["lon"])
            self.address = q[0]["display_name"]
            self.address_parts = q[0]["address"]
            self.save()
            return self.lat, self.lng
        except IndexError:
            return 0, 0

    def get_long_address(self):
        return self.address

    def get_short_address(self):
        """
        Returns a short address string from the address detail object.
        Standard display-ready address has the following format:
          <road>, <suburb>, <village>, <city>, <state>, <postcode>, <country>
        Short version will have the following format:
          <road>, <suburb>, <village>, <city>
        """
        if not self.address_parts:
            return self.address
        parts = [
            self.address_parts[k].split(" - ")[0]
            for k in ("road", "house_number", "village", "city") if k in self.address_parts
        ]
        o = ", ".join(parts)
        if o:
            return o
        if "municipality" in self.address_parts:
            return self.address_parts["municipality"]
        return self.address_parts

    def get_coordinates(self):
        if self.lat and self.lng:
            return self.lat, self.lng
        return self.get_coordinates_from_OSM()

    def get_print_address(self):
        if self.name:
            return f"{self.name} ({self.get_short_address() or self.address})"
        return self.get_short_address()

    def get_b64_address(self):
        return base64.b64encode((self.__str__() or "").encode()).decode("utf-8")

    def get_b64_print_address(self):
        return base64.b64encode((self.get_print_address() or "").encode()).decode("utf-8")

    def __str__(self):
        if not self.name:
            return self.address
        return f'{self.name} ({self.address})'

    class Meta:
        verbose_name = _("Location")
        verbose_name_plural = _("Locations")
        ordering = ('address', 'name')


class Senior(models.Model):
    """
    Customers of this service.
    """

    user = models.ForeignKey(
        "auth.User",
        on_delete=models.PROTECT,
        verbose_name=_("user")
    )

    yob = models.IntegerField(
        choices=[(y, str(y)) for y in range(
            datetime.date.today().year - 120,
            datetime.date.today().year
        )],
        blank=True,
        null=True,
        verbose_name=_("year of birth")
    )

    phone_number = models.CharField(
        max_length=15,
        blank=True,
        null=True,
        verbose_name=_("phone number")
    )

    contact_sms = models.BooleanField(
        default=False,
        verbose_name=_("Contact via SMS"),
        help_text=_("Make sure the phone number is of a Mobile if this option is enabled")
    )

    contact_email = models.BooleanField(
        default=False,
        verbose_name=_("Contact via EMail")
    )

    language = models.TextField(
        max_length=10,
        choices=settings.LANGUAGES,
        default=settings.LANGUAGE_CODE,
        verbose_name=_("Contact language")
    )

    home_address = models.ForeignKey(
        "moveaid.Location",
        on_delete=models.PROTECT,
        verbose_name=_("home address")
    )

    member_card_issuer = models.ForeignKey(
        "moveaid.Organization",
        on_delete=models.PROTECT,
        verbose_name=_("member card issuer"),
        null=True,
        blank=True
    )

    member_card_number = models.CharField(
        max_length=20,
        verbose_name=_("member card number"),
        null=True,
        blank=True
    )

    notes = models.TextField(
        verbose_name=_("notes"),
        null=True,
        blank=True
    )

    def get_official_name(self):
        return "{} {}".format(self.user.last_name, self.user.first_name)

    __str__ = lambda self: '{} ({}#{})'.format(
        self.user.get_full_name() or self.user.username,
        (self.member_card_issuer.name if self.member_card_issuer else ''),
        (self.member_card_number if self.member_card_number else '')
    )

    class Meta:
        verbose_name = _("Senior")
        verbose_name_plural = _("Seniors")
        ordering = ('user__last_name', 'user__first_name')


class Reservation(models.Model):
    """
    Reservation request by the senior.
    """

    senior = models.ForeignKey(
        "moveaid.Senior",
        on_delete=models.PROTECT,
        verbose_name=_("senior")
    )

    notes = models.TextField(
        verbose_name=_("notes"),
        null=True,
        blank=True
    )

    needs_wheelchair = models.BooleanField(
        default=False,
        verbose_name=_("needs wheelchair vehicle")
    )

    inserted_by = models.ForeignKey(
        "moveaid.Employee",
        on_delete=models.PROTECT,
        verbose_name=_("inserted by"),
        null=True
    )

    @property
    def first_stage_time(self):
        q = self.tripstage_set.all()
        if q:
            return q[0].estimated_be_at
        return None

    @property
    def last_stage_time(self):
        q = self.tripstage_set.all().order_by("-estimated_be_at")
        if q:
            return q[0].estimated_be_at
        return None

    @property
    def get_trip_stages(self):
        return list(self.tripstage_set.all())

    @property
    def get_trip_legs(self):
        return list(self.tripleg_set.all())

    def get_b64_notes(self):
        return base64.b64encode((self.notes or "").encode()).decode("utf-8")

    __str__ = lambda self: f'{self.senior} {self.first_stage_time}'

    class Meta:
        verbose_name = _("Reservation")
        verbose_name_plural = _("Reservations")
        ordering = ["pk"]


class TripStage(models.Model):
    """
    The steps that make up the trip of a reservation.
    """

    reservation = models.ForeignKey(
        "moveaid.Reservation",
        on_delete=models.CASCADE,
        verbose_name=_("reservation")
    )

    location = models.ForeignKey(
        "moveaid.Location",
        on_delete=models.PROTECT,
        verbose_name=_("location")
    )

    number = models.IntegerField(verbose_name=_("number"))

    estimated_be_at = models.DateTimeField()

    real_be_at = models.DateTimeField(null=True, blank=True)

    __str__ = lambda self: f'{self.reservation}#{self.number}'

    class Meta:
        verbose_name = _("Trip stage")
        verbose_name_plural = _("Trips stages")
        ordering = ('estimated_be_at',)


class TripLeg(models.Model):
    """
    The actual trips that connect the trip stages within a reservation.
    """

    reservation = models.ForeignKey(
        "moveaid.Reservation",
        on_delete=models.CASCADE,
        verbose_name=_("reservation")
    )

    start = models.ForeignKey(
        "moveaid.TripStage",
        related_name='start_stage',
        on_delete=models.CASCADE,
        verbose_name=_("Start stage")
    )

    end = models.ForeignKey(
        "moveaid.TripStage",
        related_name='end_stage',
        on_delete=models.CASCADE,
        verbose_name=_("End stage")
    )

    driver_shift = models.ForeignKey(
        "moveaid.DriverShift",
        on_delete=models.PROTECT,
        null=True,
        blank=True,
        verbose_name=_("driver shift")
    )

    def get_color(self):
        if getattr(self, "driver_shift", None):
            return self.driver_shift.driver.color
        return "#121212"

    def print_A5(self):
        values = {
            "PASSENGER_FULLNAME": self.reservation.senior.user.get_full_name() or self.reservation.senior.user.username,
            "PASSENGER_PHONE": self.reservation.senior.phone_number,
            "PASSENGER_NOTES": self.reservation.senior.notes or "",
            "DRIVER": self.driver_shift.driver.user.get_full_name() or self.driver_shift.driver.user.username,
            "TARGA_AUTO": self.driver_shift.get_vehicle_plate() or "",
            "START_ADDRESS": self.start.location.get_print_address(),
            "START_DATETIME": self.start.estimated_be_at.strftime("%d/%m/%Y %H:%M"),
            "DEST_ADDRESS": self.end.location.get_print_address(),
            "DEST_DATETIME": self.end.estimated_be_at.strftime("%d/%m/%Y %H:%M"),
            "NOTES": self.reservation.notes or "-",
            "MOVEAID_VERSION": VERSION,
            "MOVEAID_REVISION": GIT_REVISION,
            "MOVEAID_INSTANCE": settings.INSTANCE_NAME
        }
        doc = opendocument.load(os.path.join(
            settings.BASE_DIR, "operators", "templates", "operators", "trip_sheet_A5.odt"
        ))
        doc_fields = doc.getElementsByType(userfield.UserFieldDecl)
        for field in doc_fields:
            try:
                name = field.getAttribute("name")
            except ValueError:
                continue
            if name in values.keys():
                field.setAttribute("stringvalue", str(values[name]).replace("\r\n", "\n"))
        _t = tempfile.NamedTemporaryFile(delete=False)
        _t.close()
        doc.save(_t.name)
        lo_path = shutil.which("libreoffice")
        if not lo_path:
            raise Exception("Can't find LibreOffice executable in PATH. Is it installed?")
        subprocess.run(
            [lo_path, "--headless", "-env:UserInstallation=file:///tmp/libreoffice", "--convert-to", "pdf", _t.name],
            cwd=os.path.dirname(_t.name),
            stdout=subprocess.DEVNULL,
            stderr=subprocess.DEVNULL,
        )
        certificate = BytesIO()
        with open(f"{_t.name}.pdf", "rb") as pdf_source:
            certificate.write(pdf_source.read())
            certificate.seek(0)
        os.unlink(f"{_t.name}.pdf")
        os.unlink(_t.name)
        return certificate

    class Meta:
        verbose_name = _("Trip leg")
        verbose_name_plural = _("Trips legs")
        ordering = ('reservation',)
