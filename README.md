# MoveAid

## Helping organizing seniors movements

[Website](https://movea.id)


## Running this project

### Clone this project

If you use an IDE, like IntelliJ IDEA or PyCharm,
he can do that for you (`New` > `Project from VCS`)

Otherwise, via CLI

```bash
git clone --recursive git@gitlab.com:lugbz/moveaid/server.git
```

... or via HTTPS ...

```bash
git clone --recursive https://gitlab.com/lugbz/moveaid/server.git
```

Always remember to set up the submodules

```bash
git submodule init
git submodule update
```

### Creating a virtual environment

Again, if you use an IDE, he can do that for you. Otherwise

```bash
python3 -m venv venv
venv/bin/pip3 install -r requirements.txt
```

### Debootstrap the database

```bash
venv/bin/python3 manage.py makemigrations
venv/bin/python3 manage.py migrate
```

### Creating a superuser

```bash
venv/bin/python3 manage.py createsuperuser
```

### Starting the development server

```bash
venv/bin/python3 manage.py runserver
```

Remember to navigate to [localhost:8000/admin](http://localhost:8000/admin),
login with the credentials created before and create yourself
a senior or an operator.


## Docker

### Initialize the database

```bash
./docker/run.sh docker/init.sh
```

### Start the server

```bash
./docker/run.sh
```

The server is reachable at the address [localhost:8000](http://localhost:8000)

## Changes in the commit

Modified:
- `README.md`
- `.gitignore`: git ignores the `/db` directory
- `moveaid/settings.py`: database can update his path if `DEFAULT_DB_NAME` is set in the env.

Added:
- `docker/Dockerfile`: used to build the moveaid-server
- `docker/build.sh`: build the server image
- `docker/run.sh`: run the server container
- `docker/init.sh`: initialize the dbs
- `docker/Dockerfile.dockerignore`: Ignore the `db/` dir during the build

## Test the pipeline container

```bash
docker run \
    --pull always \
    --rm \
    -it \
    --name moveaid-server \
    -p 127.0.0.1:8000:8000 \
    -v ./db:/db:rw \
    registry.gitlab.com/lugbz/moveaid/server:latest  $@
```


## Old

```
## Web API Implementation

The Web API always answers with a JSON. All POST requests require
a CSRF Token passed via `csrfmiddlewaretoken`.


### Status

Every request has both an HTTP status and a status label.

HTTP status can currently be:
- 200: Ok
- 400: Bad request
- 401: Login required
- 405: Method not supported

The status label is represented with the `status` key in the
JSON returned dictionary. The status label can currently be:
- `ok`: API call processed successfully
- `error`: API call cannot be processed correctly. See `error` field for
  further informations.
- `success`: API call has been processed correctly
- `fail`: API call was correctly interpreted but some of the provided data
  was wrong or incompatible with the request.


### Currently implemented API calls

1. GET `/api/web/csrf`:

   Get the CSRF token.

   Sample output: `{"status": "ok", "token": "- a long string -"}`

2. POST `/api/web/login`:

   Login user with given credentials.

   Form: `webapi.forms.LoginForm`

   Parameters:
   - username
   - password

   Possible outputs:
   - `{"msg": "Login successfull", "status": "success"}`
   - `{"msg": "Wrong username or password", "status": "fail"}`

3. GET `/api/web/login/is_authenticated`

   Returns if the user is authenticated or not.

   Sample output: `{"is_authenticated": false, "status": "ok"}`

4. GET `/api/web/login/logout`

   Login required. Logs the user out.

   Sample output: `{"msg": "Goodbye!", "status": "success"}`
```