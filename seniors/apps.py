from django.apps import AppConfig


class SeniorsConfig(AppConfig):
    name = 'seniors'
