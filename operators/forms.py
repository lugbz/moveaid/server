# This file is part of the MoveAid project.
# Copyright (C) 2021 Marco Marinello <marco.marinello@lugbz.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from django import forms
from django.contrib.auth.models import User
from django.forms import widgets
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _
from django.core.exceptions import ValidationError

from moveaid.models import Reservation, TripStage, Location, Senior, DriverShift, Employee, Vehicle
from moveaid.utils import sanitize_phone_number
from moveaid.validators import check_future_date, check_trip_leg_list


import datetime


class AddFormControlToAllTextFields:
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for i in self.fields:
            field_type = getattr(self.fields[i].widget, "input_type", None)
            if field_type == "text":
                self.fields[i].widget.attrs.update({'class': 'form-control'})
            elif field_type == "select":
                self.fields[i].widget.attrs.update({'class': 'form-select'})


class ReservationForm(forms.ModelForm):
    reservation_date = forms.DateField(
        label=_("Reservation date"),
        required=True,
        widget=forms.DateInput(attrs={
            'type': 'date',
            'class': 'my-2 rounded w-100',
            'min': datetime.date.today().isoformat()
        }),
        validators=[
            check_future_date
        ]
    )
    trip_legs = forms.JSONField(
        required=True,
        widget=forms.HiddenInput(),
        error_messages={
            'required': _("Insert at least one trip leg to complete the reservation")
        },
        validators=[
            check_trip_leg_list
        ]
    )

    class Meta:
        model = Reservation
        fields = ("senior", "reservation_date", "notes", "needs_wheelchair")
        widgets = {"senior": forms.HiddenInput, "notes": forms.Textarea(attrs={'style': 'width: 100%'})}

    def get_location(self, data):
        if "pk" in data and data["pk"]:
            return Location.objects.get(pk=data["pk"])
        return Location.objects.create(**data)

    def save(self, commit=True):
        instance = super().save(commit=True)
        
        instance.tripleg_set.all().delete()
        for trip_leg in self.cleaned_data['trip_legs']:
            instance.tripleg_set.create(
                start=instance.tripstage_set.create(
                    location=self.get_location(trip_leg['start'].pop('location')),
                    estimated_be_at=datetime.datetime.combine(
                        self.cleaned_data['reservation_date'],
                        datetime.time.fromisoformat(trip_leg['start'].pop('estimated_be_at'))
                    ),
                    **trip_leg['start']
                ),
                end=instance.tripstage_set.create(
                    location=self.get_location(trip_leg['end'].pop('location')),
                    estimated_be_at=datetime.datetime.combine(
                        self.cleaned_data['reservation_date'],
                        datetime.time.fromisoformat(trip_leg['end'].pop('estimated_be_at'))
                    ),
                    **trip_leg['end']
                )
            )
        if commit:
            instance.save()
        return instance


class AddTripStageForm(forms.ModelForm):
    class Meta:
        model = TripStage
        fields = ("reservation", "location", "number")


class LocationForm(forms.ModelForm):
    lat = forms.CharField(widget=forms.TextInput(attrs={"class": "form-control", "readonly": ""}))
    lng = forms.CharField(widget=forms.TextInput(attrs={"class": "form-control", "readonly": ""}))

    def clean(self):
        out = super().clean()
        for e in ["lat", "lng"]:
            if e in out:
                if len(out[e]) > 9:
                    out[e] = out[e][:9]
        return out

    class Meta:
        model = Location
        fields = ("name", "address", "lat", "lng")
        widgets = {
            "name": forms.TextInput(attrs={"class": "form-control"})
        }


class SeniorForm(AddFormControlToAllTextFields, forms.ModelForm):
    email = forms.CharField(required=False)
    first_name = forms.CharField(label=_("First name"))
    last_name = forms.CharField(label=_("Last name"))
    home_address = forms.CharField(label=_("Home address"))
    home_address_parts = forms.JSONField(
        label=_("Home address parts"), 
        widget=forms.HiddenInput()
    )
    home_address_lat = forms.CharField(widget=forms.TextInput(attrs={"readonly": ""}))
    home_address_lng = forms.CharField(widget=forms.TextInput(attrs={"readonly": ""}))
    notes = forms.CharField(widget=forms.Textarea(
        attrs={
            "class": "w-100"
        }
    ))
    next_url = forms.CharField(widget=forms.HiddenInput(), required=False)

    def __init__(self, *args, **kw):
        if kw.get("instance", None):
            initial = kw.get("initial", {})
            if hasattr(kw["instance"], "user"):
                initial["email"] = kw["instance"].user.email
                initial["first_name"] = kw["instance"].user.first_name
                initial["last_name"] = kw["instance"].user.last_name
            if hasattr(kw["instance"], "home_address"):
                initial["home_address"] = kw["instance"].home_address.address
                initial["home_address_parts"] = kw["instance"].home_address.address_parts
                initial["home_address_lat"] = kw["instance"].home_address.lat
                initial["home_address_lng"] = kw["instance"].home_address.lng
            kw["initial"] = initial
        super().__init__(*args, **kw)

    def clean(self):
        out = super().clean()
        for e in ["home_address_lat", "home_address_lng"]:
            if e in out:
                if len(out[e]) > 9:
                    out[e] = out[e][:9]
        return out

    def clean_phone_number(self):
        if self.cleaned_data["phone_number"]:
            return sanitize_phone_number(self.cleaned_data["phone_number"])
        return None

    def save(self, commit=True):
        instance = super().save(commit=False)
        if not hasattr(instance, "home_address"):
            instance.home_address = Location()
        if not hasattr(instance, "user"):
            instance.user = User()
            if self.cleaned_data.get("email"):
                instance.user.username = self.cleaned_data["email"]
            else:
                instance.user.username = f"{self.cleaned_data['first_name']}_{self.cleaned_data['last_name']}".lower()
        if self.cleaned_data["home_address"] != instance.home_address.address:
            new_home = Location.objects.create(
                address=self.cleaned_data["home_address"],
                address_parts=self.cleaned_data["home_address_parts"],
                lat=self.cleaned_data["home_address_lat"],
                lng=self.cleaned_data["home_address_lng"]
            )
            instance.home_address = new_home
        for field in ("email", "first_name", "last_name"):
            if getattr(instance.user, field, None) != self.cleaned_data[field]:
                setattr(instance.user, field, self.cleaned_data[field])
        instance.user.save()
        if commit:
            instance.save()
        return instance

    class Meta:
        model = Senior
        fields = ("yob", "phone_number", "contact_sms", "contact_email", "language",
                  "member_card_issuer", "member_card_number", "notes")


class VehicleForm(AddFormControlToAllTextFields, forms.ModelForm):
    car_model = forms.CharField()

    class Meta:
        model = Vehicle
        fields = ("plate_number", "car_model", "wheelchair",
                  "owner", "user", "active")


class DriverShiftEditForm(AddFormControlToAllTextFields, forms.ModelForm):
    time_start = forms.DateTimeField(
        widget=forms.DateTimeInput(attrs={
            'type': 'datetime-local',
            'class': 'w-100',
            'min': datetime.date.today().isoformat()
        }),
        validators=[
            check_future_date
        ]
    )
    time_end = forms.DateTimeField(
        widget=forms.DateTimeInput(attrs={
            'type': 'datetime-local',
            'class': 'w-100',
            'min': datetime.date.today().isoformat()
        }),
        validators=[
            check_future_date
        ]
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["driver"].queryset = Employee.objects.filter(is_driver=True)

    def clean(self):
        if len(self._errors) > 0:
            return self.cleaned_data
        if self.cleaned_data["time_start"] > self.cleaned_data["time_end"]:
            raise ValidationError({
                "time_start": _("Make sure the start of the shift is set to be earlier than the end of the shift.")
            })
        return self.cleaned_data

    class Meta:
        model = DriverShift
        fields = ("driver", "vehicle", "time_start", "time_end")
