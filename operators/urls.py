# This file is part of the MoveAid project.
# Copyright (C) 2021 Marco Marinello <marco.marinello@lugbz.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from django.urls import path

from operators import views

urlpatterns = [
    path('', views.DashboardView.as_view(), name="dashboard"),
    path('reservation/new', views.NewReservationView.as_view(), name='new-reservation'),
    path('reservation/new/', views.NewReservationView.as_view(), name='new-reservation-from-trip-leg'),
    path('reservation/new/<int:tlid>', views.NewReservationView.as_view(), name='new-reservation-from-trip-leg'),
    path('reservation/<int:pk>', views.EditReservationView.as_view(), name='edit-reservation'),
    path('reservation/', views.EditReservationView.as_view(), name='edit-reservation'),
    path('reservation/trip_leg/delete/<int:pk>', views.DeleteTripLegView.as_view(), name='delete-trip-leg'),
    path('reservation/trip_leg/delete/', views.DeleteTripLegView.as_view(), name='delete-trip-leg'),
    path('reservation/ajax_query', views.ReservationsQuery.as_view(), name='reservation-ajax-query'),
    path('senior/ajax_query', views.SeniorQuery.as_view(), name='senior-ajax-query'),
    path('location/', views.LocationsList.as_view(), name='locations-list'),
    path('location/problematic/', views.ProblematicLocationsList.as_view(), name='problematic-locations-list'),
    path(
        'location/problematic/<int:pk>/edit',
        views.EditProblematicLocationView.as_view(),
        name='edit-problematic-locations-list'
    ),
    path('location/new', views.NewLocationView.as_view(), name='new-location'),
    path('location/<int:pk>/edit', views.EditLocationView.as_view(), name='edit-location'),
    path('location/<int:pk>/delete', views.DeleteLocationView.as_view(), name='delete-location'),
    path('senior/', views.SeniorSearchView.as_view(), name='seniors'),
    path('senior/new', views.NewSeniorView.as_view(), name='new-senior'),
    path('senior/<int:pk>/edit', views.EditSeniorView.as_view(), name='edit-senior'),
    path('vehicles/', views.VehiclesList.as_view(), name='vehicles-list'),
    path('vehicles/new', views.NewVehicleView.as_view(), name='new-vehicle'),
    path('vehicles/<int:pk>/edit', views.EditVehicleView.as_view(), name='edit-vehicle'),
    path('vehicles/<int:pk>/delete', views.DeleteVehicleView.as_view(), name='delete-vehicle'),
    path('vehicles/pair', views.VehiclePairDevice.as_view(), name='pair-vehicle'),
    path('drivers_shifts', views.DriversShiftsList.as_view(), name='drivers-shifts'),
    path('drivers_shifts/new', views.DriverShiftCreation.as_view(), name='driver-shift-new'),
    path('drivers_shifts/old', views.DriversShiftsOldList.as_view(), name='drivers-shifts-old'),
    path('drivers_shifts/<int:pk>', views.DriverShiftUpdate.as_view(), name='driver-shift-update'),
    path('drivers_shifts/<int:pk>/delete', views.DriverShiftDelete.as_view(), name='driver-shift-delete'),
    path('drivers_shifts/<int:pk>/print', views.ManyTripSheetPrint.as_view(), name='driver-shift-print-A5'),
    path('driver/ajax_query', views.DriverQuery.as_view(), name='driver-ajax-query'),
    path('vehicle/ajax_query', views.VehicleQuery.as_view(), name='vehicle-ajax-query'),
    path('reservation/trip_leg/assign', views.TripLegDriverAssociation.as_view(), name='trip-leg-driver-association'),
    path('reservation/trip_leg/deassign', views.TripLegDriverDeassociation.as_view(),
         name='trip-leg-driver-deassociation'),
    path('reservation/trip_leg/print/', views.TripSheetPrint.as_view(), name='trip-sheet-print'),
    path('reservation/trip_leg/print/<int:pk>', views.TripSheetPrint.as_view(), name='trip-sheet-print'),
    path('vehicles_rt/', views.VehiclesRealTime.as_view(), name='vehicles-rt'),
    path('vehicles_rt/ajax', views.VehiclesRealTimeAjax.as_view(), name='vehicles-rt-ajax'),
    path('info', views.PlatformInfo.as_view(), name='info'),
]
