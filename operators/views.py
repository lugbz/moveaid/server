# This file is part of the MoveAid project.
# Copyright (C) 2021 Marco Marinello <marco.marinello@lugbz.org>
# Copyright (C) 2021 Andrea Esposito <aespositox@gmail.com>
# Copyright (C) 2021-2022 Marco Marinello <marco@marinello.bz.it>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import datetime
import json
import math
from functools import reduce
from io import BytesIO

from PyPDF2 import PdfMerger
from django.conf import settings
from django.contrib import messages
from django.contrib.messages.views import SuccessMessageMixin
from django.db.models import Q
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.http.response import Http404, HttpResponseBadRequest
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _
from django.views import View
from django.views.generic import ListView, FormView, CreateView, UpdateView, DeleteView, TemplateView
from django.views.generic.detail import SingleObjectMixin, BaseDetailView
from django.shortcuts import get_object_or_404

from moveaid.models import Reservation, Senior, Employee, Location, Organization, TripLeg, Vehicle, DriverShift
from operators.forms import ReservationForm, LocationForm, SeniorForm, DriverShiftEditForm, VehicleForm
from operators.mixins import OperatorRequiredMixin
from operators.utils import generate_base64_qrcode
from webapi.utils import get_user_roles


class DashboardView(OperatorRequiredMixin, ListView):
    template_name = "operators/dashboard.html"
    model = Reservation


class NewReservationView(OperatorRequiredMixin, FormView):
    template_name = "operators/reservation_new.html"
    form_class = ReservationForm

    def form_valid(self, form):
        reservation = form.save(commit=False)
        reservation.inserted_by = self.request.user.employee_set.all()[0]
        reservation.save()
        messages.success(self.request, _("New reservation saved correctly."))
        return HttpResponseRedirect(reverse("operators:dashboard"))

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if "tlid" not in self.kwargs:
            return context
        tl = get_object_or_404(TripLeg, pk=self.kwargs["tlid"])
        context["source"] = tl.reservation
        return context


class EditReservationView(OperatorRequiredMixin, SuccessMessageMixin, UpdateView):
    template_name = "operators/reservation_new.html"
    form_class = ReservationForm
    model = Reservation    
    success_message = _("Reservation successfully updated.")

    def get_success_url(self):
        return reverse("operators:dashboard")


class SeniorQuery(OperatorRequiredMixin, View):
    def post(self, request):
        query = request.POST.get("q", "")
        if not query or len(query) < 2:
            return JsonResponse([], safe=False)
        # Looking for card no.
        if query.isdigit():
            qs = Senior.objects.filter(member_card_number__startswith=int(query))[:5]
        # Search by last name
        else:
            qs = Senior.objects.filter(user__last_name__istartswith=query)[:5]
        return JsonResponse([{
            "pk": o.pk,
            "id": o.pk,
            "fullname": o.user.get_full_name(),
            "member_card_issuer": o.member_card_issuer.name if o.member_card_issuer else None,
            "member_card_number": o.member_card_number or None,
            "phone_number": o.phone_number or None,
            "email": o.user.email or None,
            "home": {
                "address": o.home_address.address,
                "lat": float(o.home_address.get_coordinates()[0]),
                "lng": float(o.home_address.lng),
            }
        } for o in qs], safe=False)


class DriverQuery(OperatorRequiredMixin, View):
    def post(self, request):
        query = request.POST.get("q", "")
        trip_start = request.POST.get("trip_start", None)
        try:
            trip_start = datetime.datetime.fromisoformat(trip_start)
        except:
            return HttpResponseBadRequest(_('Malformed start date of trip'))
        trip_end = request.POST.get("trip_end", None)
        try:
            trip_end = datetime.datetime.fromisoformat(trip_end)
        except:
            return HttpResponseBadRequest(_('Malformed end date of trip'))
        if not query or len(query) < 2:
            return JsonResponse([], safe=False)
        qd = Employee.objects.filter(
            is_driver=True,
            user__first_name__istartswith=query
        ) | Employee.objects.filter(
            is_driver=True,
            user__last_name__istartswith=query
        )
        qd = qd[:5]

        return JsonResponse([{
            "id": o.pk,
            "fullname": o.user.get_full_name(),
            "hasSuitableDriverShift": o.drivershift_set.filter(
                time_start__lte=trip_start,
                time_end__gte=trip_end
            ).exists(),
            "disabled": not o.drivershift_set.filter(
                time_start__lte=trip_start,
                time_end__gte=trip_end
            ).exists(),
            "isAssignedToSimultaneousTripLeg": o.drivershift_set.filter(
                tripleg__start__estimated_be_at__lte=trip_start,
                tripleg__end__estimated_be_at__gte=trip_end
            ).exists() or o.drivershift_set.filter(
                tripleg__start__estimated_be_at__gte=trip_start,
                tripleg__end__estimated_be_at__lte=trip_end,
            ).exists() or o.drivershift_set.filter(
                tripleg__start__estimated_be_at__lte=trip_start,
                tripleg__end__estimated_be_at__lte=trip_end,
                tripleg__end__estimated_be_at__gte=trip_start
            ).exists() or o.drivershift_set.filter(
                tripleg__start__estimated_be_at__gte=trip_start,
                tripleg__end__estimated_be_at__gte=trip_end,
                tripleg__start__estimated_be_at__lte=trip_end
            ).exists(),
            "driverShifts": [{
                "id": ds.pk,
                "driver": ds.driver.user.get_full_name(),
                "vehicle": str(ds.vehicle),
                "timeStart": ds.time_start.isoformat(),
                "timeEnd": ds.time_end.isoformat()
            } for ds in o.drivershift_set.filter(
                time_start__lte=trip_start,
                time_end__gte=trip_end
            )]
        } for o in qd], safe=False)


class VehicleQuery(OperatorRequiredMixin, View):
    def post(self, request):
        query = request.POST.get("q", "")
        qv = Vehicle.objects.filter(
            active=True
        )
        if query:
            qv = qv.filter(
                plate_number__icontains=query
            )
        if not self.request.user.is_staff:
            qv = qv.filter(owner=self.request.user.employee_set.first().organization)
        return JsonResponse([{
            "id": o.pk,
            "plate_number": o.plate_number,
            "car_model": o.car_model,
            "wheelchair": o.wheelchair
        } for o in qv], safe=False)


class TripLegDriverAssociation(OperatorRequiredMixin, View):
    def post(self, request):
        trip_leg = request.POST.get("t", None)
        driver_shift = request.POST.get("ds", None)

        try:
            trip_leg = TripLeg.objects.get(pk=trip_leg)
            driver_shift = DriverShift.objects.get(pk=driver_shift)
        except:
            return HttpResponseBadRequest(_('Missing or invalid driver_shift or trip_leg'))

        trip_leg.driver_shift = driver_shift
        trip_leg.save()

        return HttpResponse(status=200)


class TripLegDriverDeassociation(OperatorRequiredMixin, View):
    def post(self, request):
        trip_leg = request.POST.get("t", None)
        try:
            trip_leg = TripLeg.objects.get(pk=trip_leg)
        except:
            return HttpResponseBadRequest(_('Missing or invalid driver_shift'))

        trip_leg.driver_shift = None
        trip_leg.save()

        return HttpResponse(status=200)


class DeleteTripLegView(OperatorRequiredMixin, DeleteView):
    template_name = "operators/trip_leg_delete.html"
    model = TripLeg

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["last_trip_leg"] = self.object.reservation.tripleg_set.count() == 1
        return context

    def get_success_url(self):
        messages.success(self.request, _("Trip leg deleted successfully."))
        return reverse("operators:dashboard")


class ReservationsQuery(OperatorRequiredMixin, View):
    def get_queryset(self):
        user_roles = get_user_roles(self.request.user)
        query = Reservation.objects.all()
        if user_roles == ["driver"]:
            query = query.filter(tripleg__driver_shift__driver__user=self.request.user)
        if self.request.POST.get("start"):
            query = query.filter(tripstage__estimated_be_at__gte=datetime.datetime.fromisoformat(
                self.request.POST.get("start")
            ).replace(tzinfo=None))
        if self.request.POST.get("end"):
            query = query.filter(tripstage__estimated_be_at__lte=datetime.datetime.fromisoformat(
                self.request.POST.get("end")
            ).replace(tzinfo=None))
        return query.distinct()

    def export_trip_legs(self, reservation):
        return [{
            "title": reservation.senior.get_official_name(),
            "start": datetime.datetime.isoformat(trip_leg.start.estimated_be_at),
            "end": datetime.datetime.isoformat(trip_leg.end.estimated_be_at),
            "url": "#",
            "backgroundColor": trip_leg.get_color(),
            "startTripStage": {
                "name": trip_leg.start.location.name,
                "address": trip_leg.start.location.address,
                "shortAddress": trip_leg.start.location.get_short_address(),
                "estimatedTime": datetime.datetime.isoformat(trip_leg.start.estimated_be_at)
            },
            "endTripStage": {
                "name": trip_leg.end.location.name,
                "address": trip_leg.end.location.address,
                "shortAddress": trip_leg.end.location.get_short_address(),
                "estimatedTime": datetime.datetime.isoformat(trip_leg.end.estimated_be_at)
            },
            "driverShift": {
                "driver": trip_leg.driver_shift.driver.user.get_full_name() if trip_leg.driver_shift and trip_leg.driver_shift.driver else None,
                "vehicle": str(
                    trip_leg.driver_shift.vehicle) if trip_leg.driver_shift and trip_leg.driver_shift.vehicle else None
            },
            "tlid": trip_leg.pk,
            "rid": reservation.pk,
            "phone_number": reservation.senior.phone_number
        } for trip_leg in reservation.get_trip_legs]

    def post(self, request):
        return HttpResponse(json.dumps(reduce(list.__add__, [
            self.export_trip_legs(a) for a in self.get_queryset()
        ], [])))


class LocationsList(OperatorRequiredMixin, ListView):
    template_name = "operators/locations.html"
    model = Location
    extra_context = {"MAP_DEFAULT_VIEW": settings.MAP_DEFAULT_VIEW}

    def get_queryset(self):
        return Location.objects.exclude(name=None).exclude(name="").distinct()


class ProblematicLocationsList(OperatorRequiredMixin, ListView):
    template_name = "operators/locations.html"
    model = Location
    extra_context = {"MAP_DEFAULT_VIEW": settings.MAP_DEFAULT_VIEW, "problematic": True}

    def get_queryset(self):
        return Location.objects.filter(
            Q(lat__isnull=True) | Q(lng__isnull=True) | Q(address_parts__isnull=True)
        ).distinct()


class NewLocationView(OperatorRequiredMixin, CreateView):
    template_name = "operators/location_new.html"
    form_class = LocationForm
    model = Location

    def get_success_url(self):
        return reverse("operators:locations-list")


class EditLocationView(OperatorRequiredMixin, SuccessMessageMixin, UpdateView):
    template_name = "operators/location_new.html"
    form_class = LocationForm
    model = Location

    def get_queryset(self):
        return Location.objects.exclude(name=None).exclude(name="").distinct()

    def get_success_message(self, cleaned_data):
        return _("{} updated successfully.").format(self.object.name)

    def get_success_url(self):
        return reverse("operators:locations-list")


class EditProblematicLocationView(EditLocationView):
    extra_context = {"problematic": True}

    def get_queryset(self):
        return Location.objects.filter(
            Q(lat__isnull=True) | Q(lng__isnull=True) | Q(address_parts__isnull=True) | Q(address_parts="")
        ).distinct()

    def get_success_url(self):
        self.object.get_coordinates_from_OSM()
        return reverse("operators:problematic-locations-list")


class DeleteLocationView(OperatorRequiredMixin, DeleteView):
    template_name = "operators/location_delete.html"
    model = Location

    def get_queryset(self):
        return Location.objects.exclude(name=None).exclude(name="").distinct()

    def get_success_url(self):
        messages.success(self.request, _("{} deleted successfully.").format(self.object.name))
        return reverse("operators:locations-list")


class SeniorSearchView(OperatorRequiredMixin, TemplateView):
    template_name = "operators/seniors.html"

    def get_context_data(self, **kwargs):
        organization_members = []
        for o in Organization.objects.exclude(id=9999):
            organization_members.append([o.name, len(o.senior_set.all())])
        width = math.ceil(len(organization_members) * 3 / 12) * 12
        factor = int(width / len(organization_members))
        return super().get_context_data(
            **kwargs,
            organization_members=organization_members,
            factor=factor,
            total_members=len(Senior.objects.all())
        )


class EditSeniorView(OperatorRequiredMixin, SuccessMessageMixin, UpdateView):
    template_name = "operators/senior_new.html"
    form_class = SeniorForm
    model = Senior
    extra_context = {"MAP_DEFAULT_VIEW": settings.MAP_DEFAULT_VIEW}

    def get_success_message(self, cleaned_data):
        return _("{} updated successfully.").format(self.object.user.get_full_name() or self.object.user.username)

    def get_success_url(self):
        return reverse("operators:seniors")


class NewSeniorView(OperatorRequiredMixin, SuccessMessageMixin, CreateView):
    template_name = "operators/senior_new.html"
    form_class = SeniorForm
    model = Senior
    extra_context = {"MAP_DEFAULT_VIEW": settings.MAP_DEFAULT_VIEW}

    def get_form_kwargs(self):
        out = super().get_form_kwargs()
        out["initial"]["next_url"] = self.request.GET.get("next", None)
        return out

    def get_success_message(self, cleaned_data):
        return _("{} created successfully.").format(self.object.user.get_full_name() or self.object.user.username)

    def form_valid(self, form):
        self.object = form.save()
        if form.cleaned_data["next_url"]:
            return HttpResponseRedirect(form.cleaned_data["next_url"])
        return super().form_valid(form)

    def get_success_url(self):
        return reverse("operators:seniors")


class EditVehicleView(OperatorRequiredMixin, SuccessMessageMixin, UpdateView):
    template_name = "operators/vehicle_new.html"
    form_class = VehicleForm
    model = Vehicle

    def get_success_message(self, cleaned_data):
        return _("{} updated successfully.").format(self.object.plate_number)

    def get_success_url(self):
        return reverse("operators:vehicles-list")


class NewVehicleView(OperatorRequiredMixin, SuccessMessageMixin, CreateView):
    template_name = "operators/vehicle_new.html"
    form_class = VehicleForm
    model = Vehicle

    def get_success_message(self, cleaned_data):
        return _("{} created successfully.").format(self.object.plate_number)

    def get_success_url(self):
        return reverse("operators:vehicles-list")


class DeleteVehicleView(OperatorRequiredMixin, DeleteView):
    template_name = "operators/vehicle_delete.html"
    model = Vehicle

    def get_success_url(self):
        messages.success(self.request, _("Vehicle successfully deleted."))
        return reverse("operators:vehicles-list")


class PlatformInfo(OperatorRequiredMixin, TemplateView):
    template_name = "operators/info.html"


class VehiclesList(OperatorRequiredMixin, ListView):
    template_name = "operators/vehicles.html"
    model = Vehicle
    extra_context = {
        "download_app_qr": generate_base64_qrcode("https://movea.id/android/app-latest.apk")
    }

    def get_queryset(self):
        q = super().get_queryset()
        if not self.request.user.is_staff:
            return q.filter(owner=self.request.user.employee_set.first().organization)
        return q


class VehiclePairDevice(OperatorRequiredMixin, SingleObjectMixin, View):
    model = Vehicle

    def post(self, request):
        if "pk" not in request.POST:
            # FIXME: Should be an error
            return HttpResponse()
        self.kwargs["pk"] = request.POST["pk"]
        self.object = self.get_object()
        return HttpResponse(generate_base64_qrcode(json.dumps(self.get_qr_data())))

    def get_qr_data(self):
        # FIXME: Add minimum app version
        pw = self.object.set_return_password()
        return {
            "base_url": f"{self.request.scheme}://{self.request.get_host()}",
            "username": self.object.user.username,
            "password": pw
        }


class DriversShiftsList(OperatorRequiredMixin, ListView):
    model = DriverShift
    template_name = "operators/drivers_shifts.html"
    extra_context = {"title": _("Today's and future shifts")}

    def get_queryset(self):
        return super().get_queryset().filter(time_start__gte=datetime.datetime.today().date())


class DriversShiftsOldList(OperatorRequiredMixin, ListView):
    model = DriverShift
    template_name = "operators/drivers_shifts.html"
    extra_context = {"title": _("Past shifts")}

    def get_queryset(self):
        return super().get_queryset().filter(time_start__lt=datetime.datetime.today().date())


class DriverShiftCreation(OperatorRequiredMixin, SuccessMessageMixin, CreateView):
    template_name = "operators/driver_shift_edit.html"
    form_class = DriverShiftEditForm
    success_message = _("Shift successfully created.")

    def get_success_url(self):
        return reverse("operators:drivers-shifts")


class DriverShiftUpdate(OperatorRequiredMixin, SuccessMessageMixin, UpdateView):
    template_name = "operators/driver_shift_edit.html"
    form_class = DriverShiftEditForm
    model = DriverShift
    success_message = _("Shift successfully updated.")

    def get_success_url(self):
        return reverse("operators:drivers-shifts")


class DriverShiftDelete(OperatorRequiredMixin, DeleteView):
    template_name = "operators/driver_shift_delete.html"
    form_class = DriverShiftEditForm
    model = DriverShift

    def get_success_url(self):
        messages.success(self.request, _("Shift successfully deleted."))
        return reverse("operators:drivers-shifts")


class TripSheetPrint(OperatorRequiredMixin, BaseDetailView):
    model = TripLeg

    def render_to_response(self, context):
        if not self.object.driver_shift:
            raise Http404()
        return HttpResponse(self.object.print_A5().read(), content_type='application/pdf')


class ManyTripSheetPrint(OperatorRequiredMixin, BaseDetailView):
    model = DriverShift

    def render_to_response(self, context):
        if not self.object.tripleg_set.all():
            raise Http404()
        merger = PdfMerger()
        for leg in self.object.tripleg_set.order_by("start__estimated_be_at"):
            merger.append(fileobj=leg.print_A5())
        out = BytesIO()
        merger.write(out)
        merger.close()
        out.seek(0)
        return HttpResponse(out.read(), content_type='application/pdf')


class VehiclesRealTime(OperatorRequiredMixin, TemplateView):
    template_name = "operators/vehicles_rt.html"
    extra_context = {"MAP_DEFAULT_VIEW": settings.MAP_DEFAULT_VIEW}


class VehiclesRealTimeAjax(OperatorRequiredMixin, View):
    def get_queryset(self, user):
        if user.is_superuser:
            return Vehicle.objects.all()
        return user.operator_set.all().first().organization.vehicle_set.all()

    def get(self, request):
        return JsonResponse([{
            "plate_number": a.plate_number,
            "car_model": a.car_model,
            "owner": a.owner.name,
            "last_pos": a.get_last_position_today()
        } for a in self.get_queryset(request.user)], safe=False)
