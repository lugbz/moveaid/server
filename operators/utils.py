# This file is part of the MoveAid project.
# Copyright (C) 2023 Marco Marinello <marco@marinello.bz.it>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import base64
from io import BytesIO

import qrcode


def generate_base64_qrcode(data):
    pil_qr = qrcode.make(data)
    buffer = BytesIO()
    pil_qr.save(buffer, format="PNG")
    buffer.seek(0)
    qr_bytes = buffer.getvalue()
    return "data:image/png;base64," + base64.b64encode(qr_bytes).decode("utf-8")
