#!/bin/sh

set -e

venv/bin/python3 manage.py makemigrations
venv/bin/python3 manage.py migrate

echo "==== Create a superuser ===="
venv/bin/python3 manage.py createsuperuser
