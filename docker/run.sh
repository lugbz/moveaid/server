#!/bin/sh

docker run \
    --rm \
    -it \
    --name moveaid-server \
    -p 127.0.0.1:8000:8000 \
    -v ./db:/db:rw \
    moveaid-image  $@
