# This file is part of the MoveAid project.
# Copyright (C) 2021 Marco Marinello <mmarinello@unibz.it>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import json
from datetime import datetime
from django.contrib.auth.models import User
from django.test import TestCase, Client
from django.utils.crypto import get_random_string

from moveaid import models
from moveaid.tests import utils


class ReservationViewSetTestCase(TestCase):
    def setUp(self):
        self.OPERATOR = User.objects.create(username="operator", is_staff=True)
        operator_obj = self.OPERATOR.employee_set.create(
            is_operator=True,
            organization=models.Organization.objects.get_or_create(name="ADA")[0]
        )
        self.DRIVER = User.objects.create(username="driver")
        driver_obj = self.DRIVER.employee_set.create(
            is_driver=True,
            organization=models.Organization.objects.get_or_create(name="ADA")[0]
        )
        self.SHIFT = driver_obj.drivershift_set.create(
            time_start=datetime(2021, 1, 1, 8),
            time_end=datetime(2021, 1, 1, 12)
        )
        fake_home = models.Location.objects.create(
            address="Fake Home Location, via che non esiste, 39100 Bolzano Italia"
        )
        self.SENIOR1 = User.objects.create(username="senior1")
        senior1_obj = self.SENIOR1.senior_set.create(home_address=fake_home)
        self.RESERVATION1 = senior1_obj.reservation_set.create(inserted_by=operator_obj)
        self.RESERVATION1.tripstage_set.create(
            location=fake_home,
            number=0,
            estimated_be_at=datetime(2021, 1, 1, 10),
        )
        self.SENIOR2 = User.objects.create(username="senior2")
        senior2_obj = self.SENIOR2.senior_set.create(home_address=fake_home)
        self.RESERVATION2 = senior2_obj.reservation_set.create(inserted_by=operator_obj)
        self.RESERVATION2.tripstage_set.create(
            location=fake_home,
            number=0,
            estimated_be_at=datetime(2021, 1, 1, 11),
        )
        self.RESERVATION3 = senior2_obj.reservation_set.create(inserted_by=operator_obj)
        self.RESERVATION3.tripstage_set.create(
            location=fake_home,
            number=0,
            estimated_be_at=datetime(2021, 1, 1, 11)
        )

    def getReservations(self, user, role):
        c = Client()
        c.force_login(user)
        c.post("/api/web/login/set_role", data={"role": role})
        return c.get("/api/web/reservations/week/2021-01-01/")

    def test_operator_can_see_all_reservations(self):
        response = self.getReservations(self.OPERATOR, "operator")
        self.assertEqual(len(response.json()), 3, "Operator cannot see all reservations")

    def test_admin_can_see_all_reservations(self):
        response = self.getReservations(self.OPERATOR, "admin")
        self.assertEqual(len(response.json()), 3, "Admin cannot see all reservations")

    def test_driver_can_only_see_shift_reservations(self):
        response = self.getReservations(self.DRIVER, "driver")
        self.assertEqual(len(response.json()), 2, "Driver cannot see all reservations")
        self.assertTrue(
            all([r["trip_stages"][0]["driver_shift"] == self.SHIFT.id for r in response.json()]),
            "Not all returned reservations in expected shift"
        )

    def test_senior1_can_only_see_his_reservations(self):
        _json = self.getReservations(self.SENIOR1, "senior").json()
        self.assertEqual(len(_json), 1, "Number of senior reservation mismatch")
        self.assertEqual(_json[0]["id"], self.RESERVATION1.id, "Reservation id mismatch")
        self.assertTrue(all([r["senior"] == self.SENIOR1.senior_set.all()[0].id for r in _json]), "Senior id mismatch")

    def test_senior2_can_only_see_his_reservations(self):
        _json = self.getReservations(self.SENIOR2, "senior").json()
        self.assertEqual(len(_json), 2, "Number of senior reservation mismatch")
        self.assertEqual(_json[0]["id"], self.RESERVATION2.id, "Reservation id mismatch")
        self.assertEqual(_json[1]["id"], self.RESERVATION3.id, "Reservation id mismatch")
        self.assertTrue(all([r["senior"] == self.SENIOR2.senior_set.all()[0].id for r in _json]), "Senior id mismatch")


class SeniorViewSetTestCase(TestCase):
    def setUp(self):
        self.OPERATOR = User.objects.create(username="operator", is_staff=True)
        self.OPERATOR_OBJ = self.OPERATOR.employee_set.create(
            is_operator=True,
            organization=models.Organization.objects.get_or_create(name="ADA")[0]
        )
        self.DRIVER = User.objects.create(username="driver")
        self.DRIVER.employee_set.create(
            is_driver=True,
            organization=models.Organization.objects.get_or_create(name="ADA")[0]
        )

        fake_home = models.Location.objects.create(
            address="Fake Home Location, via che non esiste, 39100 Bolzano Italia"
        )
        self.SENIOR = User.objects.create(username="testsenior1")
        self.SENIOR_OBJ = self.SENIOR.senior_set.create(
            home_address=fake_home
        )

        self.RELATED_DRIVER = User.objects.create(username="relateddriver")
        self.RELATED_DRIVER_OBJ = self.RELATED_DRIVER.employee_set.create(
            is_driver=True,
            organization=models.Organization.objects.get_or_create(name="ADA")[0]
        )

        self.ANOTHER_SENIOR = User.objects.create(
            username='testsenior2',
        )
        self.ANOTHER_SENIOR_OBJ = self.ANOTHER_SENIOR.senior_set.create(
            home_address=fake_home
        )

        self.NAMED_SENIOR = User.objects.create(
            username='namedsenior'
        )
        self.NAMED_SENIOR_OBJ = self.NAMED_SENIOR.senior_set.create(
            home_address=fake_home
        )

        self.SENIOR_WITH_MEMBER_CARD = User.objects.create(
            username='seniorwithmembercard'
        )
        self.SENIOR_WITH_MEMBER_CARD_OBJ = self.SENIOR_WITH_MEMBER_CARD.senior_set.create(
            home_address=fake_home,
            member_card_issuer=models.Organization.objects.get_or_create(name="ADA")[0],
            member_card_number='65587435530912908861'
        )

        self.RESERVATION = utils.generateReservation(
            self.SENIOR_OBJ, 
            self.OPERATOR_OBJ, 
            self.RELATED_DRIVER_OBJ
        )

        self.not_found_payload = {
            'detail': "Non trovato."
        }
        self.not_authorized_payload = {
            'detail': "Non hai l'autorizzazione per eseguire questa azione."
        }

    def generatePayload(self):
        return {
            "user": {
                "username": "senior_" + get_random_string(length=16, allowed_chars='1234567890'),
                "password": "test"
            },
            "phone_number": "463784123",
            "home_address": {
                "address": "Fake Home Location, via che non esiste, 39100 Bolzano Italia"
            },
            "member_card_issuer": {
                "name": "ADA"
            }
        }

    def getLoggedClientWithRole(self, user, role):
        c = Client()
        c.force_login(user)
        c.post("/api/web/login/set_role", data={"role": role})
        return c

    def createSenior(self, user, role, payload):
        c = self.getLoggedClientWithRole(user, role)
        return c.post('/api/web/seniors/', data=payload, content_type='application/json')

    def getSeniors(self, user, role):
        c = self.getLoggedClientWithRole(user, role)
        return c.get('/api/web/seniors/')

    def retrieveSenior(self, user, role, pk):
        c = self.getLoggedClientWithRole(user, role)
        return c.get('/api/web/seniors/{}/'.format(pk))

    def test_only_operator_and_admin_can_create_seniors(self):
        payloads = [self.generatePayload() for _ in range(4)]
        response1 = self.createSenior(self.SENIOR, 'senior', payloads[0]).json()
        response2 = self.createSenior(self.OPERATOR, 'operator', payloads[1]).json()
        response3 = self.createSenior(self.DRIVER, 'driver', payloads[2]).json()
        response4 = self.createSenior(self.OPERATOR, 'admin', payloads[3]).json()
        self.assertEqual(response1, self.not_authorized_payload, 'users of type senior can create new senior users')
        self.assertTrue(type(response2) == dict and response2['user']['username'] == payloads[1]['user']['username'], 'users of type operator cannot create new senior users')
        self.assertEqual(response3, self.not_authorized_payload, 'users of type driver can create new senior users')
        self.assertTrue(type(response4) == dict and response4['user']['username'] == payloads[3]['user']['username'], 'users of type admin cannot create new senior users')

    def test_nobody_can_see_all_seniors(self):
        response1 = self.getSeniors(self.SENIOR, 'senior').json()
        response2 = self.getSeniors(self.OPERATOR, 'operator').json()
        response3 = self.getSeniors(self.DRIVER, 'driver').json()
        response4 = self.getSeniors(self.OPERATOR, 'admin').json()
        self.assertEqual(response1, self.not_authorized_payload, 'users of type senior can dump all senior users')
        self.assertEqual(len(response2), 0, 'users of type operator can dump all senior users')
        self.assertEqual(len(response3), 0, 'users of type driver can dump all senior users')
        self.assertEqual(len(response4), 0, 'users of type admin can dump all senior users')

    def test_only_operator_and_admin_can_retrieve_seniors_by_pk(self):
        response1 = self.retrieveSenior(self.SENIOR, 'senior', self.ANOTHER_SENIOR_OBJ.pk).json()
        response2 = self.retrieveSenior(self.OPERATOR, 'operator', self.ANOTHER_SENIOR_OBJ.pk).json()
        response3 = self.retrieveSenior(self.DRIVER, 'driver', self.ANOTHER_SENIOR_OBJ.pk).json()
        response4 = self.retrieveSenior(self.OPERATOR, 'admin', self.ANOTHER_SENIOR_OBJ.pk).json()
        self.assertEqual(response1, self.not_authorized_payload, 'users of type senior can retrieve other seniors by pk')
        self.assertTrue(type(response2) == dict and response2['user']['username'] == self.ANOTHER_SENIOR.username, 'users of type operator cannot retrieve other seniors by pk')
        self.assertEqual(response3, self.not_found_payload, 'users of type driver can retrieve other seniors by pk')
        self.assertTrue(type(response4) == dict and response4['user']['username'] == self.ANOTHER_SENIOR.username, 'users of type admin cannot retrieve other seniors by pk')

    def test_driver_can_see_related_seniors(self):
        response = self.retrieveSenior(self.RELATED_DRIVER, 'driver', self.SENIOR_OBJ.pk).json()
        self.assertTrue(type(response) == dict and response['user']['username'] == self.SENIOR.username, 'users of type driver cannot retrieve senior users that are related to them through a reservation')