# This file is part of the MoveAid project.
# Copyright (C) 2021 Marco Marinello <mmarinello@unibz.it>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from django.contrib.auth.models import User
from django.test import TestCase, Client
from moveaid.models import Organization, Location


class LoginViewTestCase(TestCase):
    def setUp(self):
        self.TESTUSER = User.objects.create(username="testuser")
        self.TESTUSER.set_password("test1234")
        self.TESTUSER.save()

    def test_login_view(self):
        c = Client()
        response = c.post('/api/web/login', data={"username": "testuser", "password": "test1234"})
        self.assertEqual(
            (response.status_code == 200 and response.json()["status"] == "no-role"),
            True,
            "Login error"
        )

    def test_login_with_wrong_credentials_fails(self):
        c = Client()
        response = c.post('/api/web/login', data={"username": "testuser", "password": "1234test"})
        self.assertEqual(
            (response.status_code == 200 and response.json()["status"] == "fail"),
            True,
            "Login authorized even with bad username or password"
        )

    def test_user_has_admin_role_if_staff(self):
        self.TESTUSER.is_staff = True
        self.TESTUSER.is_superuser = False
        self.TESTUSER.save()
        c = Client()
        response = c.post('/api/web/login', data={"username": "testuser", "password": "test1234"})
        self.assertEqual(
            (response.status_code == 200 and response.json()["role"] == "admin"),
            True,
            "Login authorized even with bad username or password"
        )

    def test_user_has_admin_role_if_superuser(self):
        self.TESTUSER.is_staff = False
        self.TESTUSER.is_superuser = True
        self.TESTUSER.save()
        c = Client()
        response = c.post('/api/web/login', data={"username": "testuser", "password": "test1234"})
        self.assertEqual(
            (response.status_code == 200 and response.json()["role"] == "admin"),
            True,
            "Login authorized even with bad username or password"
        )

    def test_isAuthenticated_without_login(self):
        c = Client()
        response = c.get('/api/web/login/is_authenticated')
        self.assertEqual(response.json()["is_authenticated"], False, "Not logged in but isAuthenticated returns True")

    def test_isAuthenticated_with_login(self):
        c = Client()
        c.force_login(self.TESTUSER)
        response = c.get('/api/web/login/is_authenticated')
        self.assertEqual(response.json()["is_authenticated"], True, "Logged in but isAuthenticated returns False")

    def test_logout(self):
        c = Client()
        c.force_login(self.TESTUSER)
        response = c.get('/api/web/login/is_authenticated')
        self.assertEqual(response.json()["is_authenticated"], True, "Logged in but isAuthenticated returns False")
        response = c.get('/api/web/login/logout')
        self.assertEqual(response.json()["status"], "success", "Logged out request not successful")
        response = c.get('/api/web/login/is_authenticated')
        self.assertEqual(response.json()["is_authenticated"], False, "Not logged in but isAuthenticated returns True")

    def test_setRole(self):
        self.TESTUSER.employee_set.create(
            is_operator=True,
            organization=Organization.objects.get_or_create(name="ADA")[0]
        )
        c = Client()
        c.force_login(self.TESTUSER)
        response = c.post('/api/web/login/set_role', data={"role": "operator"})
        self.assertEqual(response.json()["status"], "ok", "Unable to set role")
        self.assertEqual(response.json()["role"], "operator", "role is not what we asked")
        response = c.get('/api/web/login/get_role')
        self.assertEqual(response.json()["role"], "operator", "role is not what we asked")

    def test_setRole_unexisting_role(self):
        c = Client()
        c.force_login(self.TESTUSER)
        response = c.post('/api/web/login/set_role', data={"role": "helloworld"})
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.json()["role"], ['Requested role not found'], "Unable to set role")

    def test_setRole_unauthorized_role(self):
        c = Client()
        c.force_login(self.TESTUSER)
        response = c.post('/api/web/login/set_role', data={"role": "admin"})
        self.assertEqual(response.status_code, 403)
        self.assertEqual(
            response.json()["detail"],
            'User has not admin role in his roles',
            "Role set although not authorized"
        )

    def test_auto_set_role_admin(self):
        c = Client()
        self.TESTUSER.is_staff = True
        self.TESTUSER.save()
        response = c.post('/api/web/login', data={"username": "testuser", "password": "test1234"})
        self.assertTrue(
            (response.status_code == 200 and response.json()["status"] == "success"),
            "Login error"
        )
        response = c.get('/api/web/login/get_role')
        self.assertEqual(response.json()["role"], "admin", "role is not admin as should")

    def test_no_auto_set_role_admin_and_operator(self):
        c = Client()
        self.TESTUSER.is_staff = True
        self.TESTUSER.save()
        self.TESTUSER.employee_set.create(
            organization=Organization.objects.get_or_create(name="ADA")[0],
            is_operator=True
        )
        response = c.post('/api/web/login', data={"username": "testuser", "password": "test1234"})
        self.assertTrue(
            (response.status_code == 200 and response.json()["status"] == "role-choice-needed"),
            "Login error"
        )
        response = c.get('/api/web/login/get_role')
        self.assertEqual(response.json()["role"], None, "role is not unset as should")

    def test_no_auto_set_role_operator_and_senior(self):
        c = Client()
        self.TESTUSER.employee_set.create(
            organization=Organization.objects.get_or_create(name="ADA")[0],
            is_operator=True
        )
        self.TESTUSER.senior_set.create(
            phone_number="1234567890",
            home_address=Location.objects.create(address="Test")
        )
        response = c.post('/api/web/login', data={"username": "testuser", "password": "test1234"})
        self.assertTrue(
            (response.status_code == 200 and response.json()["status"] == "role-choice-needed"),
            "Login error"
        )
        response = c.get('/api/web/login/get_role')
        self.assertEqual(response.json()["role"], None, "role is not unset as should")
