# This file is part of the MoveAid project.
# Copyright (C) 2021 Marco Marinello <mmarinello@unibz.it>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from django.contrib.auth.models import User
from django.test import TestCase
from moveaid.models import Organization, Location
from webapi import utils


class UtilsTestCase(TestCase):
    def test_superuser_has_admin_role(self):
        testuser = User.objects.create(username="testuser", is_superuser=True)
        self.assertEqual(["admin"], utils.get_user_roles(testuser), "Superuser has no admin role")

    def test_staff_has_admin_role(self):
        testuser = User.objects.create(username="testuser", is_staff=True)
        self.assertEqual(["admin"], utils.get_user_roles(testuser), "Staff has no admin role")

    def test_senior_has_senior_role(self):
        testuser = User.objects.create(username="testuser")
        testuser.senior_set.create(
            phone_number="1234567890",
            home_address=Location.objects.create(address="Test")
        )
        self.assertEqual(["senior"], utils.get_user_roles(testuser), "Senior has no senior role")

    def test_operator_has_operator_role(self):
        testuser = User.objects.create(username="testuser")
        testuser.employee_set.create(
            organization=Organization.objects.get_or_create(name="ADA")[0],
            is_operator=True
        )
        self.assertEqual(["operator"], utils.get_user_roles(testuser), "Operator has no operator role")

    def test_driver_has_driver_role(self):
        testuser = User.objects.create(username="testuser")
        testuser.employee_set.create(
            organization=Organization.objects.get_or_create(name="ADA")[0],
            is_driver=True
        )
        self.assertEqual(["driver"], utils.get_user_roles(testuser), "Driver has no driver role")

    def test_user_driver_and_operator_roles(self):
        testuser = User.objects.create(username="testuser")
        testuser.employee_set.create(
            organization=Organization.objects.get_or_create(name="ADA")[0],
            is_operator=True,
            is_driver=True
        )
        self.assertEqual(
            ["driver", "operator"],
            utils.get_user_roles(testuser),
            "Driver and operator does not have both roles"
        )

    def test_user_driver_and_admin_roles(self):
        testuser = User.objects.create(username="testuser", is_staff=True)
        testuser.employee_set.create(
            organization=Organization.objects.get_or_create(name="ADA")[0],
            is_driver=True
        )
        self.assertEqual(
            ["driver", "admin"],
            utils.get_user_roles(testuser),
            "Driver and admin does not have both roles"
        )

    def test_user_operator_and_senior_roles(self):
        testuser = User.objects.create(username="testuser")
        testuser.employee_set.create(
            organization=Organization.objects.get_or_create(name="ADA")[0],
            is_operator=True
        )
        testuser.senior_set.create(
            phone_number="1234567890",
            home_address=Location.objects.create(address="Test")
        )
        self.assertEqual(
            ["operator", "senior"],
            utils.get_user_roles(testuser),
            "Operator and senior does not have both roles"
        )
