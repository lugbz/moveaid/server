# This file is part of the MoveAid project.
# Copyright (C) 2021 Andrea Esposito <aesposito@unibz.it>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from django.contrib.auth.models import User
from rest_framework import serializers
from moveaid import models


class SeniorPrimaryKeyField(serializers.PrimaryKeyRelatedField):
    def get_queryset(self):
        return models.Senior.objects.all()

class DriverShiftPrimaryKeyField(serializers.PrimaryKeyRelatedField):
    def get_queryset(self):
        return models.DriverShift.objects.all()

class EmployeePrimaryKeyField(serializers.PrimaryKeyRelatedField):
    def get_queryset(self):
        return models.Employee.objects.all()

class ReservationPrimaryKeyField(serializers.PrimaryKeyRelatedField):
    def get_queryset(self):
        return models.Reservation.objects.all()

class OrganizationPrimaryKeyField(serializers.PrimaryKeyRelatedField):
    def get_queryset(self):
        return models.Organization.objects.all()

class VehiclePrimaryKeyField(serializers.PrimaryKeyRelatedField):
    def get_queryset(self):
        return models.Vehicle.objects.all()

class UserPrimaryKeyField(serializers.PrimaryKeyRelatedField):
    def get_queryset(self):
        return User.objects.all()