# This file is part of the MoveAid project.
# Copyright (C) 2021 Marco Marinello <mmarinello@unibz.it>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from rest_framework.routers import SimpleRouter
from . import rest

router = SimpleRouter()
# FIXME: Check the point of this view
router.register(r'seniors', rest.SeniorViewSet, basename='seniors')
router.register(r'reservations', rest.ReservationsViewSet, basename='reservations')
router.register(r'shifts', rest.DriverShiftsViewSet, basename='shifts')
router.register(r'vehicles', rest.VehiclesViewSet, basename='vehicles')
router.register(r'profile', rest.UserProfilePageViewSet, basename='profile')
