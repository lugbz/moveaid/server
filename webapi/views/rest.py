# This file is part of the MoveAid project.
# Copyright (C) 2021 Andrea Esposito <aesposito@unibz.it>
# Copyright (C) 2021 Marco Marinello <mmarinello@unibz.it>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from datetime import date, timedelta
from rest_framework import status, viewsets
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.exceptions import APIException
from moveaid import models
from webapi import serializers
from webapi.permissions import RoleSetRequired, IsOperator, IsSenior, IsAdmin, IsDriver, IsReading
from webapi.serializers import UserProfilePageSerializer


class SeniorViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.SeniorSerializer
    permission_classes = [IsAdmin | IsOperator | (IsDriver & IsReading)]

    def get_queryset(self):
        if self.request.session["role"] in ["operator", "admin"]:
            return models.Senior.objects.all()
        elif self.request.session["role"] == "driver":
            return models.Senior.objects.filter(reservation__tripstage__driver_shift__driver__user=self.request.user)
        elif self.request.session["role"] == "senior":
            return models.Reservation.objects.filter(senior__user=self.request.user)

    def list(self, request):
        return Response([])

    @action(detail=False, url_name='by_member_card', url_path=r'by_member_card/(?P<query>[0-9]{1,20})')
    def by_member_card(self, request, query=None):
        return Response(self.get_serializer(
            self.get_queryset().filter(member_card_number=query),
            many=True
        ).data)

    @action(detail=False, url_name='by_name', url_path=r'by_name/(?P<query>[\w ]{3,64})')
    def by_name(self, request, query=None):
        return Response(self.get_serializer(
            self.get_queryset().filter(user__last_name__icontains=query),
            many=True
        ).data)


class ReservationsViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.ReservationSerializer
    permission_classes = [IsAuthenticated & RoleSetRequired]

    def get_queryset(self):
        if self.request.session["role"] in ["operator", "admin"]:
            return models.Reservation.objects.all()
        elif self.request.session["role"] == "driver":
            return models.Reservation.objects.filter(tripstage__driver_shift__driver__user=self.request.user)
        elif self.request.session["role"] == "senior":
            return models.Reservation.objects.filter(senior__user=self.request.user)

    @action(detail=False, url_name='week', url_path=r'week/(?P<timestamp>[0-9\-]{10})')
    def week(self, request, timestamp=None):
        serializer = serializers.TimestampSerializer(data={
            "timestamp": timestamp
        })
        if serializer.is_valid():
            week_start = serializer.validated_data['timestamp']
            if week_start.weekday() != 0:
                week_start -= timedelta(days=week_start.weekday())
            week_end = week_start + timedelta(days=6)
            return Response(
                self.get_serializer(
                    self.get_queryset().filter(
                        tripstage__estimated_be_at__range=[week_start, week_end]
                    ), many=True
                ).data
            )
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @action(methods=['post'], detail=False, url_name='assign',
            url_path=r'(?P<pk>[0-9]{1,256})/assign/(?P<trip_stage_nr>[0-9]{1,256})/(?P<driver_shift_pk>[0-9]{1,256})')
    def assign_driver_shift_to_trip_stage(self, request, pk=None, trip_stage_nr=None, driver_shift_pk=None):
        try:
            reservation = models.Reservation.objects.get(id=pk)
            trip_stage = models.TripStage.objects.get(number=trip_stage_nr, reservation__id=pk)
            driver_shift = models.DriverShift.objects.get(id=driver_shift_pk)
        except:
            raise APIException('Oggetti richiesti non trovati.')
        trip_stage.driver_shift = driver_shift
        trip_stage.save()

        reservation.refresh_from_db()
        return Response(self.get_serializer(
            reservation
        ).data)


class DriverShiftsViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.DriverShiftSerializer
    permission_classes = [IsAuthenticated & RoleSetRequired]

    def get_queryset(self):
        if self.request.session["role"] in ["operator", "admin"]:
            return models.DriverShift.objects.all()
        elif self.request.session["role"] == "driver":
            return models.DriverShift.objects.filter(driver__user=self.request.user)
        elif self.request.session["role"] == "senior":
            return models.DriverShift.objects.none()


class VehiclesViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.VehicleSerializer
    permission_classes = [IsAuthenticated & RoleSetRequired]

    def get_queryset(self):
        if self.request.session["role"] in ["operator", "admin"]:
            return models.Vehicle.objects.all()
        elif self.request.session["role"] == "driver":
            return models.Vehicle.objects.filter(drivershift__driver__user=self.request.user)
        elif self.request.session["role"] == "senior":
            return models.Vehicle.objects.filter(drivershift__tripstage__reservation__senior__user=self.request.user)

    @action(detail=False, url_name='by_organization', url_path=r'by_organization/(?P<pk>[0-9]{1,256})')
    def by_organization(self, request, pk=None):
        return Response(self.get_serializer(
            self.get_queryset().filter(owner__id=pk),
            many=True
        ).data)


class UserProfilePageViewSet(viewsets.ViewSet):
    serializer_class = serializers.ReservationSerializer
    permission_classes = [IsAuthenticated & RoleSetRequired]

    def get_object(self):
        return self.request.user

    def get_serializer_args(self, instance):
        return dict(
            first_name=instance.first_name,
            last_name=instance.last_name,
            home_address=None if not instance.senior_set.all().exists() else instance.senior_set.all()[0].home_address.address,
            email=instance.email,
            member_card_issuer=None if not instance.senior_set.all().exists() else instance.senior_set.all()[0].member_card_issuer,
            member_card_number=None if not instance.senior_set.all().exists() else instance.senior_set.all()[0].member_card_number,
            language=None if not instance.senior_set.all().exists() else instance.senior_set.all()[0].language,
        )

    def list(self, request, pk=None):
        return Response(UserProfilePageSerializer(self.get_serializer_args(request.user), many=False).data)

    retrieve = list

    def update(self, request, pk=None):
        serializer = self.serializer(self.get_serializer_args(request.user), data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data)
