# This file is part of the MoveAid project.
# Copyright (C) 2021 Marco Marinello <mmarinello@unibz.it>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from django.contrib.auth import authenticate, login, logout
from django.middleware.csrf import get_token
from rest_framework.exceptions import PermissionDenied
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from webapi.serializers import LoginSerializer, RoleSerializer
from webapi.permissions import CSRFTokenRequired
from webapi.utils import get_user_roles, set_role, get_role_object_for_session


class GetCSRFTokenView(APIView):
    """
    Return the CSRF token to be included in POST or other data-put requests
    """
    def get(self, request):
        return Response({"token": get_token(self.request)})


class LoginView(APIView):
    """
    Login via SessionAuthentication system using username and password

    :returns:
        Possible status codes:

        - fail: Wrong username or password

        - success: Correct username and password, role set (see `role` variable)

        - role-choice-needed: Correct username and password but multiple roles for this user.
          Please set one with an appropriate call. In this case an additional variable `roles` is returned.

        - no-role: There is no role linked with this user
    """
    permission_classes = [CSRFTokenRequired]

    def post(self, request):
        s = LoginSerializer(data=request.data)
        s.is_valid(raise_exception=True)
        user = authenticate(
            self.request,
            username=s.validated_data["username"],
            password=s.validated_data["password"]
        )
        if user is not None:
            login(request, user)
            roles = get_user_roles(user)
            if len(roles) > 1:
                return Response({
                    "msg": "Login successful",
                    "status": "role-choice-needed",
                    "roles": roles
                })
            if len(roles) == 0:
                return Response({
                    "msg": "Login successful, but no role associated with user",
                    "status": "no-role"
                })
            set_role(request, roles[0])
            return Response(
                {"msg": "Login successful", "status": "success", "role": roles[0]}
            )
        return Response({"msg": "Wrong username or password", "status": "fail"})


class LogoutView(APIView):
    """
    Logout a SessionAuthentication authenticated session
    """
    permission_classes = [IsAuthenticated]

    def get(self, request):
        logout(self.request)
        return Response({"msg": "Goodbye!", "status": "success"})


class IsAuthenticatedView(APIView):
    """
    Answer with current session status
    """
    def get(self, request):
        return Response({"status": "ok", "is_authenticated": request.user.is_authenticated})


class SetRoleView(APIView):
    """
    Set the session variable with users' role

    :param: role
    :returns: status ok or exception if role is invalid or not applicable for the user
    """

    permission_classes = [IsAuthenticated]

    def post(self, request):
        s = RoleSerializer(data=request.data)
        s.is_valid(raise_exception=True)
        role = s.validated_data["role"]
        if role not in get_user_roles(request.user):
            raise PermissionDenied("User has not {} role in his roles".format(role))
        set_role(request, role)
        return Response({
            "status": "ok", 
            "role": role,
            "role_object": get_role_object_for_session(request)
        })


class GetRoleView(APIView):
    """
    Answer with current session status
    """
    def get(self, request):
        return Response({
            "status": "ok", 
            "role": request.session.get("role"),
            "role_object": get_role_object_for_session(request)
        })


class GetAllUserRolesView(APIView):
    """
    Return all roles the user has; use case: client side role switcher 
    """
    permission_classes = [IsAuthenticated]
    
    def get(self, request):
        return Response({
            "status": "ok",
            "roles": get_user_roles(request.user)
        })