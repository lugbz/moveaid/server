# This file is part of the MoveAid project.
# Copyright (C) 2021 Andrea Esposito <aesposito@unibz.it>
# Copyright (C) 2021 Marco Marinello <mmarinello@unibz.it>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from django.middleware.csrf import CsrfViewMiddleware
from rest_framework import permissions
from rest_framework.exceptions import PermissionDenied
from rest_framework.permissions import SAFE_METHODS

from moveaid.models import Reservation
from webapi.utils import get_user_roles, ALL_ROLES, set_role


class CSRFTokenRequired(permissions.BasePermission):
    """
    Check CSRF Token in POST requests
    """

    # implemented from django.middleware.csrf.CsrfViewMiddleware
    def has_permission(self, request, view):
        mwcl = CsrfViewMiddleware()
        mwcl.process_view(request, lambda x: None, [], {})
        return getattr(request, "csrf_processing_done", False)


class RoleSetRequired(permissions.BasePermission):
    """
    Check if the role-session variable has been set and is coherent with users' privileges
    """

    def check_admin_role(self, user):
        return user.is_staff or user.is_superuser

    def check_operator_role(self, user):
        return user.employee_set.filter(is_operator=True).exists()

    def check_driver_role(self, user):
        return user.employee_set.filter(is_driver=True).exists()

    def check_senior_role(self, user):
        return user.senior_set.all().exists()

    def has_permission(self, request, view):
        if not request.session.get("role", False):
            roles = get_user_roles(request.user)
            if len(roles) > 1 or len(roles) == 0:
                raise PermissionDenied("Role variable is not set and cannot be guessed")
            set_role(request, roles[0])
        if request.session["role"] not in ALL_ROLES:
            raise PermissionDenied("Role is invalid")
        return getattr(self, "check_{}_role".format(request.session["role"]))(request.user)


class IsAdmin(RoleSetRequired):
    """
        Convenience permission class to ensure the admin role is available 
        and currently selected for the current user.
    """

    def has_permission(self, request, view):
        return super().has_permission(request, view) and request.session['role'] == 'admin'

    def has_object_permission(self, request, view, obj):
        return self.has_permission(request, view)

class IsOperator(RoleSetRequired):
    """
        Convenience permission class to ensure the operator role is available 
        and currently selected for the current user.
    """

    def has_permission(self, request, view):
        return super().has_permission(request, view) and request.session['role'] == 'operator'

    def has_object_permission(self, request, view, obj):
        return self.has_permission(request, view)

class IsDriver(RoleSetRequired):
    """
        Convenience permission class to ensure the driver role is available 
        and currently selected for the current user.
    """

    def has_permission(self, request, view):
        return super().has_permission(request, view) and request.session['role'] == 'driver'

    def has_object_permission(self, request, view, obj):
        return self.has_permission(request, view)

class IsSenior(RoleSetRequired):
    """
        Convenience permission class to ensure the senior role is available 
        and currently selected for the current user.
    """

    def has_permission(self, request, view):
        return super().has_permission(request, view) and request.session['role'] == 'senior'

    def has_object_permission(self, request, view, obj):
        return self.has_permission(request, view)
        

class IsReading(permissions.BasePermission):
    """
        Convenience permission class to ensure the user is attempting a 
        read operation.
    """
    def has_permission(self, request, view):
        return request.method in SAFE_METHODS

    def has_object_permission(self, request, view, obj):
        return self.has_permission(request, view)