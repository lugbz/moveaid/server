# This file is part of the MoveAid project.
# Copyright (C) 2021 Marco Marinello <mmarinello@unibz.it>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


ALL_ROLES = ["driver", "operator", "senior", "admin"]

from . import serializers


def get_user_roles(user):
    roles = []
    if not hasattr(user, 'employee_set') or not hasattr(user, 'senior_set'):
        return roles
    if user.employee_set.all().exists():
        employee = user.employee_set.all().first()
        if employee.is_driver:
            roles.append("driver")
        if employee.is_operator:
            roles.append("operator")
    if user.senior_set.all().exists():
        roles.append("senior")
    if user.is_staff or user.is_superuser:
        roles.append("admin")
    return roles


def get_role_object_for_session(request):
    role = request.session.get("role")
    if not role:
        return None
    if role in ["operator", "driver"]:
        return serializers.EmployeeSerializer(request.user.employee_set.first()).data
    elif role == "senior":
        return serializers.SeniorSerializer(request.user.senior_set.first()).data
    elif role == "admin":
        return serializers.EmployeeSerializer(request.user.employee_set.get_or_create(is_operator=True)[0]).data
    else:
        return None


def set_role(request, role):
    request.session["role"] = role
