# This file is part of the MoveAid project.
# Copyright (C) 2021 Andrea Esposito <aesposito@unibz.it>
# Copyright (C) 2021 Marco Marinello <mmarinello@unibz.it>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from django.contrib.auth.models import User
from django.conf import settings
from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from moveaid import models
from . import fields
from . import utils

ALL_ROLES = utils.ALL_ROLES


class TimestampSerializer(serializers.Serializer):
    timestamp = serializers.DateField()


class LocationSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Location
        fields = '__all__'


class OrganizationSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Organization
        fields = '__all__'


class UserSerializer(serializers.ModelSerializer):
    password = serializers.CharField(max_length=128, write_only=True)

    class Meta:
        model = User
        fields = ['username', 'password', 'first_name', 'last_name', 'email']


class EmployeeSerializer(serializers.ModelSerializer):
    user = UserSerializer()
    organization = OrganizationSerializer()
    is_driver = serializers.BooleanField()
    is_operator = serializers.BooleanField()

    class Meta:
        model = models.Employee
        fields = '__all__'


class SeniorSerializer(serializers.ModelSerializer):
    user = UserSerializer()
    home_address = LocationSerializer()
    member_card_issuer = OrganizationSerializer()

    class Meta:
        model = models.Senior
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(SeniorSerializer, self).__init__(*args, **kwargs)

        request = self.context.get('request')
        if request:
            fields = request.query_params.get('fields')
            if fields:
                fields = fields.split(',')
                allowed = set(fields)
                existing = set(self.fields.keys())
                for field_name in existing - allowed:
                    self.fields.pop(field_name)

    def create(self, validated_data):
        user_data = validated_data.pop('user')
        user = User.objects.create_user(**user_data)
        home_address_data = validated_data.pop('home_address')
        home_address = models.Location.objects.create(**home_address_data)
        member_card_issuer_data = validated_data.pop('member_card_issuer')
        member_card_issuer = models.Organization.objects.create(**member_card_issuer_data)
        senior = models.Senior.objects.create(
            user=user,
            home_address=home_address,
            member_card_issuer=member_card_issuer,
            **validated_data
        )
        senior.save()
        return senior


class DriverShiftSerializer(serializers.ModelSerializer):
    driver = fields.EmployeePrimaryKeyField()
    vehicle = fields.VehiclePrimaryKeyField(allow_null=True, required=False)

    class Meta:
        model = models.DriverShift
        fields = '__all__'

    def to_representation(self, obj):
        if 'driver' in self.fields.keys():
            self.fields['driver'] = EmployeeSerializer(
                source='get_driver',
                many=False
            )
        if 'vehicle' in self.fields.keys():
            self.fields['vehicle'] = VehicleSerializer(
                source='get_vehicle',
                many=False
            )
        return super().to_representation(obj)


class VehicleSerializer(serializers.ModelSerializer):
    owner = fields.OrganizationPrimaryKeyField()
    user = fields.UserPrimaryKeyField(allow_null=True)

    class Meta:
        model = models.Vehicle
        fields = '__all__'


class TripStageSerializer(serializers.ModelSerializer):
    reservation = fields.ReservationPrimaryKeyField(read_only=True)
    location = LocationSerializer()

    class Meta:
        model = models.TripStage
        fields = '__all__'


class TripLegSerializer(serializers.ModelSerializer):
    start = TripStageSerializer()
    end = TripStageSerializer()
    driver_shift = fields.DriverShiftPrimaryKeyField()

    class Meta:
        model = models.TripLeg
        fields = '__all__'


class ReservationSerializer(serializers.ModelSerializer):
    senior = fields.SeniorPrimaryKeyField()
    inserted_by = fields.EmployeePrimaryKeyField(allow_null=True, required=False)
    trip_stages = TripStageSerializer(many=True)

    class Meta:
        model = models.Reservation
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(ReservationSerializer, self).__init__(*args, **kwargs)

        request = self.context.get('request')
        if request:
            fields = request.query_params.get('fields')
            if fields:
                fields = fields.split(',')
                allowed = set(fields)
                existing = set(self.fields.keys())
                for field_name in existing - allowed:
                    self.fields.pop(field_name)

    def create(self, validated_data):
        trip_stages_data = validated_data.pop('trip_stages')
        r = models.Reservation.objects.create(**validated_data)
        [models.TripStage.objects.create(
            reservation_id=r.pk,
            location=models.Location.objects.create(
                **trip_stage_data.pop('location')
            ),
            **trip_stage_data
        ).save() for trip_stage_data in trip_stages_data]

        r.save()
        return r

    def to_representation(self, obj):
        if 'trip_stages' in self.fields.keys():
            self.fields['trip_stages'] = TripStageSerializer(
                source='get_trip_stages',
                many=True
            )
        return super().to_representation(obj)

    def validate_trip_stages(self, stages):
        if len(stages) == 0:
            raise serializers.ValidationError("At least one trip stage per reservation is required.")
        return stages


class LoginSerializer(serializers.Serializer):
    username = serializers.CharField()
    password = serializers.CharField()

    class Meta:
        fields = ["username", "password"]


class RoleSerializer(serializers.Serializer):
    role = serializers.CharField()

    def validate_role(self, value):
        if value not in ALL_ROLES:
            raise ValidationError("Requested role not found")
        return value

    class Meta:
        fields = ["role"]


class UserProfilePageSerializer(serializers.Serializer):
    first_name = serializers.CharField(required=False)
    last_name = serializers.CharField(required=False)
    home_address = serializers.CharField(required=False)
    email = serializers.EmailField(required=False)
    member_card_issuer = serializers.CharField(required=False, read_only=False)
    member_card_number = serializers.CharField(required=False, read_only=False)
    language = serializers.ChoiceField(choices=settings.LANGUAGES, default=settings.LANGUAGE_CODE)

    def update(self, instance, validated_data):
        user_roles = utils.get_user_roles(instance)
        for i in ["first_name", "last_name", "email"]:
            setattr(instance, i, validated_data.get(i, getattr(instance, i)))
        instance.save()
        if "senior" in user_roles:
            senior = instance.senior_set.all()[0]
            home_location = senior.home_address
            if validated_data.get("home_address", home_location.address) != home_location.address:
                home_location.address = validated_data.get("home_address")
                home_location.save()
            for i in ["language"]:
                setattr(senior, i, validated_data.get(i, getattr(senior, i)))
            senior.save()
