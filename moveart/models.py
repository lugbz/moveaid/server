# This file is part of the MoveAid project.
# Copyright (C) 2023 Marco Marinello <contact-nohuman@marinello.bz.it>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from django.db import models
from django.utils.translation import ugettext_lazy as _


class VehiclePosition(models.Model):
    vehicle = models.ForeignKey("moveaid.Vehicle", on_delete=models.PROTECT, verbose_name=_("vehicle"))

    lat = models.DecimalField(
        max_digits=9,
        decimal_places=6,
        null=True,
        blank=True,
        verbose_name=_("latitude")
    )

    lng = models.DecimalField(
        max_digits=9,
        decimal_places=6,
        null=True,
        blank=True,
        verbose_name=_("longitude")
    )

    timestamp = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = _("Vehicle position")
        verbose_name_plural = _("Vehicles positions")
        ordering = ('-timestamp', 'vehicle')
