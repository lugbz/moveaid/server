# This file is part of the MoveAid project.
# Copyright (C) 2023 Marco Marinello <contact-nohuman@marinello.bz.it>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from django.contrib.auth.models import User
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework.response import Response

from moveart.models import VehiclePosition


class VehiclePositionSubmission(APIView):
    permission_classes = [IsAuthenticated]

    def post(self, request):
        try:
            vehicle = request.user.vehicle
        except User.vehicle.RelatedObjectDoesNotExist:
            return Response(
                {
                    "message": "Say no to body shaming! Even if you're fat, you won't be a vehicle.",
                    "error": "no-associated-vehicle"
                }, status=status.HTTP_400_BAD_REQUEST
            )
        VehiclePosition.objects.create(
            vehicle=vehicle,
            lat=float(request.POST.get("lat")),
            lng=float(request.POST.get("lng"))
        )
        return Response(
            {
                "message": "OK",
                "error": None
            },
            status=status.HTTP_200_OK
        )


class LastUserPosition(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request):
        try:
            vehicle = request.user.vehicle
        except User.vehicle.RelatedObjectDoesNotExist:
            return Response(
                {
                    "message": "Say no to body shaming! Even if you're fat, you won't be a vehicle.",
                    "error": "no-associated-vehicle"
                }, status=status.HTTP_400_BAD_REQUEST
            )
        vp = vehicle.vehicleposition_set.order_by("-timestamp").first()
        return Response(
            {
                "position": {
                    "lat": vp.lat,
                    "lng": vp.lng,
                    "timestamp": vp.timestamp
                } if vp else None,
                "message": "OK",
                "error": None
            },
            status=status.HTTP_200_OK
        )


class VerifyToken(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request):
        return Response(
            {
                "message": "OK",
                "error": None
            },
            status=status.HTTP_200_OK
        )