from django.apps import AppConfig


class MoveartConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'moveart'
